﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Domain;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Services;
using Xcalibur.SCRIBE.Core.Settings;
using Xcalibur.SCRIBE.Infrastructure.Domain;

namespace Xcalibur.SCRIBE.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private IQueryDispatcher QueryDispatcher { get; set; }
        private IDatabaseSettings DatabaseSettings { get; set; }

        public UserService(IDatabaseSettings databaseSettings, IQueryDispatcher queryDispatcher)
        {
            DatabaseSettings = databaseSettings;
            QueryDispatcher = queryDispatcher;
        }

        public UserDto GetUser(string username)
        {
            SearchUserQuery query = new SearchUserQuery { Username = username };
            return QueryDispatcher.Dispatch<SearchUserQuery, UserDto>(query).SingleOrDefault();
        }

        public UserDto GetUser(int userId)
        {
            SearchUserQuery query = new SearchUserQuery { UserId = userId };
            return QueryDispatcher.Dispatch<SearchUserQuery, UserDto>(query).SingleOrDefault();
        }

        public UserWithPermissionsDto GetUserWithPermissions(int userId)
        {
            UserDto user = GetUser(userId);
            return GetUserWithPermissions(user);
        }

        public UserWithPermissionsDto GetUserWithPermissions(string username)
        {
            UserDto user = GetUser(username);
            return GetUserWithPermissions(user);
        }

        public UserWithPermissionsDto UpdateDefaultInstanceAndGetUser(int userId, int instanceId)
        {

            throw new NotImplementedException();
        }

        public UserLoginDto ValidateUserLogin(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                return null;

            SearchUserLoginQuery query = new SearchUserLoginQuery {Username = username};
            var user = QueryDispatcher.Dispatch<SearchUserLoginQuery, UserLoginDto>(query).SingleOrDefault();

            if (user == null)
                return null;

            string hashedPassword = CreateOneWayHash(password, user.Salt);
            if (!hashedPassword.Equals(user.Password))
                user.IsPasswordValid = false;

            user.Password = string.Empty;
            user.Salt = string.Empty;

            return user;
        }

        private UserWithPermissionsDto GetUserWithPermissions(UserDto userDto)
        {
            if (userDto == null)
                return null;

            UserWithPermissionsDto userWithPermissionDto = new UserWithPermissionsDto();
            userWithPermissionDto.UserId = userDto.UserId;
            userWithPermissionDto.UserName = userDto.Username;
            userWithPermissionDto.FirstName = userDto.FirstName;
            userWithPermissionDto.LastName = userDto.LastName;
            userWithPermissionDto.InstanceIdDefault = userDto.InstanceIdDefault;

            //CDO: Add Permissions
            IEnumerable<UserPermissionDto> permissions = GetUserPermissions(userDto.UserId);
            userWithPermissionDto.UserPermissions = permissions;

            userWithPermissionDto.IsSystemAdmin = GetIsSystemAdmin(permissions);
            userWithPermissionDto.HasExpenseReportPermissionOnly = GetHasExpenseReportPermissionOnly(permissions, userDto.InstanceIdDefault);

            return userWithPermissionDto;
        }

        private bool GetHasExpenseReportPermissionOnly(IEnumerable<UserPermissionDto> list, int? instanceId)
        {
            if (instanceId == null)
                return false;
            //CDO: GetHasExpenseReportPermissionOnly is hardcoded to false;
            return false;

        }

        private bool GetIsSystemAdmin(IEnumerable<UserPermissionDto> list)
        {
            return true;
            //CDO: GetIsSystemAdmin - remove hardcoded = true 
            //return list.Where(x => x.EntityTypeId == (int)enmEntitytType.System && x.PermissionId == (int)enmPermission.SystemAdminAccess).Any();
        }

        private IEnumerable<UserPermissionDto> GetUserPermissions(int userId)
        {
            SearchUserPermissionQuery query = new SearchUserPermissionQuery {UserId = userId};
            return QueryDispatcher.Dispatch<SearchUserPermissionQuery, UserPermissionDto>(query);
        }

        private static string CreateOneWayHash(string stringToHash, string salt)
        {
            var saltAndHash = String.Concat(stringToHash, salt);
            var sha = new System.Security.Cryptography.SHA512Managed();
            var saltBytes = Encoding.ASCII.GetBytes(saltAndHash);
            var result = sha.ComputeHash(saltBytes);

            return Convert.ToBase64String(result);
        }

        /// <summary>
        /// Record a failed login attempt.
        /// </summary>
        /// <param name="userId">Id of User</param>
        /// <param name="maxFailedAttemptsAllowed">Max number of failed login attempts until user account is locked</param>
        /// <returns>Whether failed login attempt resulting in the user account being locked</returns>
        public bool RecordFailedLogin(int userId, int maxFailedAttemptsAllowed)
        {
            using (SCRIBEDbContext ctx = new SCRIBEDbContext(DatabaseSettings))
            {
                User user = ctx.Users.Find(userId);
                if (user == null)
                    return false;

                user.FailedLoginAttempts += 1;

                if (user.FailedLoginAttempts >= maxFailedAttemptsAllowed)
                {
                    user.IsLocked = true;
                    user.DateLocked = DateTime.Now;
                    user.ReasonLocked = "Too many failed login attempts.";
                }

                ctx.SaveChanges();
                return user.IsLocked;
            }
        }

        /// <summary>
        /// Record a successful login
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="ipAddressLastLogin">IP address of user</param>
        public void RecordSuccessfulLogin(int userId, string ipAddressLastLogin)
        {
            using (SCRIBEDbContext ctx = new SCRIBEDbContext(DatabaseSettings))
            {
                User user = ctx.Users.Find(userId);
                if (user == null)
                    return;
                user.FailedLoginAttempts = 0;
                user.IPAddressLastLogin = ipAddressLastLogin;
                user.DateLastLogin = DateTime.Now;

                ctx.SaveChanges();

            }
        }
    }

    
}
