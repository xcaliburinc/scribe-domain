﻿using System.Linq;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Services;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Services
{
    public class InstanceService : IInstanceService
    {
        #region Constructors

        public InstanceService(IDatabaseSettings databaseSettings, IQueryDispatcher queryDispatcher)
        {
            DatabaseSettings = databaseSettings;
            QueryDispatcher = queryDispatcher;
        }

        #endregion

        #region Private Properties

        private IDatabaseSettings DatabaseSettings { get; set; }

        private IQueryDispatcher QueryDispatcher { get; set; }

        #endregion

        #region Public Methods

        public InstanceDto GetInstance(int instanceId)
        {
            SearchInstanceQuery query = new SearchInstanceQuery { InstanceId = instanceId };
            return QueryDispatcher.Dispatch<SearchInstanceQuery, InstanceDto>(query).SingleOrDefault();
        }

        #endregion
    }
}
