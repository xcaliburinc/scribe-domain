﻿using System.Collections.Generic;
using System.Text;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class UserQueryHandler : BaseQueryHandler, IQueryHandler<SearchUserQuery, UserDto>
    {
        public UserQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings) { }

        public IEnumerable<UserDto> Retrieve(SearchUserQuery query)
        {
            return GetList<UserDto>(GenerateSql(query), query);
        }

        private static string GenerateSql(SearchUserQuery query)
        {
            const string sqlTemplate =
@"SELECT u.UserId, 
	u.FirstName,
	u.MiddleName,
	u.LastName,
	u.EmailAddress,
	u.UserName, 
	u.IsRequiredToChangePassword, 
	u.IsActive, 
	u.IsLocked, 
	u.DateLocked, 
    u.ReasonLocked,
	u.DateCreated, 
	u.CreationMethodId,
	qCreationModificationMethod.ModificationMethodName AS CreationMethodName, 
	u.CreatedByUserId,
	CASE WHEN qCreatedByUser.UserId IS NOT NULL THEN qCreatedByUser.FirstName + ' ' + qCreatedByUser.LastName ELSE NULL END AS CreatedByUserName,
	u.DateLastUpdated, 
	u.LastUpdateMethodId, 
	qUpdateModificationMethod.ModificationMethodName AS LastUpdateMethodName,
	u.LastUpdatedByUserId, 
	CASE WHEN qUpdatedByUser.UserId IS NOT NULL THEN qUpdatedByUser.FirstName + ' ' + qUpdatedByUser.LastName ELSE NULL END AS UpdatedByUserName,
	u.IPAddressLastLogin, 
	u.DateLastLogin, 
	u.CanRequestPassword, 
	u.FailedLoginAttempts,  
	u.WorkPhone, 
	u.MobilePhone,
    qDefaultInstance.InstanceId InstanceIdDefault
FROM [User] u 
    LEFT OUTER JOIN ( 
		SELECT ui.UserId, MIN(ui.InstanceId) InstanceId
		FROM UserToInstance ui 
        WHERE ui.IsDefault = 1
		GROUP BY ui.UserId
	) qDefaultInstance ON u.UserId = qDefaultInstance.UserId
	LEFT OUTER JOIN (
		SELECT u.UserId, u.FirstName, u.LastName
		FROM [User] u) qCreatedByUser ON qCreatedByUser.UserId = u.CreatedByUserId
	LEFT OUTER JOIN (
		SELECT u.UserId, u.FirstName, u.LastName
		FROM [User] u) qUpdatedByUser ON qUpdatedByUser.UserId = u.LastUpdatedByUserId
	LEFT OUTER JOIN (
		SELECT ModificationMethod.ModificationMethodId, ModificationMethod.ModificationMethodName 
		FROM ModificationMethod) qCreationModificationMethod ON qCreationModificationMethod.ModificationMethodId = u.CreationMethodId
	LEFT OUTER JOIN (
		SELECT ModificationMethod.ModificationMethodId, ModificationMethod.ModificationMethodName 
		FROM ModificationMethod) qUpdateModificationMethod ON qUpdateModificationMethod.ModificationMethodId = u.CreationMethodId
WHERE 1 = 1 ";

            var sbWhere = new StringBuilder();
            if (query.UserId.HasValue)
                sbWhere.Append("AND u.UserId = @UserId ");
            if (!string.IsNullOrEmpty(query.Username))
                sbWhere.Append("AND u.Username = @Username ");
            if (!string.IsNullOrEmpty(query.FirstName))
                sbWhere.Append("AND u.FirstName LIKE '%' + @FirstName + '%' ");
            if (!string.IsNullOrEmpty(query.LastName))
                sbWhere.Append("AND u.LastName LIKE '%' + @LastName + '%' ");
            if (!string.IsNullOrEmpty(query.WorkPhone))
                sbWhere.Append("AND u.WorkPhone LIKE '%' + @WorkPhone + '%' ");
            if (!string.IsNullOrEmpty(query.MobilePhone))
                sbWhere.Append("AND u.MobilePhone LIKE '%' + @MobilePhone + '%' ");
            if (!string.IsNullOrEmpty(query.EmailAddress))
                sbWhere.Append("AND u.EmailAddress LIKE '%' + @EmailAddress + '%' ");
            if (query.IsActive.HasValue)
                sbWhere.Append("AND u.IsActive = @IsActive ");
            if (query.IsLocked.HasValue)
                sbWhere.Append("AND u.IsLocked = @IsLocked ");
            if (query.IsRequiredToChangePassword.HasValue)
                sbWhere.Append("AND u.IsRequiredToChangePassword = @IsRequiredToChangePassword ");
            if (query.DateLockedEnd.HasValue)
                sbWhere.Append("AND u.DateLocked < DATEADD(d,1,@DateLockedEnd) ");
            if (query.DateLockedStart.HasValue)
                sbWhere.Append("AND u.DateLocked >= @DateLockedStart ");
            if (query.DateLastLoggedInEnd.HasValue)
                sbWhere.Append("AND u.DateLastLoggedInEnd < DATEADD(d,1,@DateLastLoggedInEnd) ");
            if (query.DateLastLoggedInStart.HasValue)
                sbWhere.Append("AND u.DateLastLoggedInStart >= @DateLastLoggedInStart ");

            return sqlTemplate + sbWhere;
        }
    }
}

