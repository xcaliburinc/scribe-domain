﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Domain;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class CourseQueryHandler : BaseQueryHandler, IQueryHandler<SearchCourseQuery, CourseDto>
    {
        public CourseQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings)
        {
        }

        public IEnumerable<CourseDto> Retrieve(SearchCourseQuery query)
        {
            return GetList<CourseDto>(GenerateSql(query), query);
        }

        private static string GenerateSql(SearchCourseQuery query)
        {
            string distinct = "";
            string schoolInfoColumns = ", SchoolId, SchoolName";
            string schoolYearInfoColumns = ", SchoolYear, SchoolYearDescription";


            var sbWhere = new StringBuilder("InstanceId = @InstanceId");

            if (!string.IsNullOrEmpty(query.CourseNumber))
                sbWhere.Append(" AND CourseNumber LIKE '%' + @CourseNumber + '%'");

            if (!string.IsNullOrEmpty(query.CourseName))
                sbWhere.Append(" AND CourseName LIKE '%' + @CourseName + '%'");

            if (query.CourseTypeQueryItem != null)
                sbWhere.Append(" AND CourseTypeId = @CourseTypeId");

            if (query.CourseLevelQueryItem != null)
                sbWhere.Append(" AND CourseLevelId = @CourseLevelId");

            if (query.ScoringMethodQueryItem != null)
                sbWhere.Append(" AND ScoringMethodId = @ScoringMethodId");

            if (query.CourseResultGroupQueryItem != null)
                sbWhere.Append(" AND CourseResultGroupId = @CourseResultGroupId");

            if (query.StandardCourseQueryItem != null)
                sbWhere.Append(" AND StandardCourseId = @StanadardCourseId");

            if (query.IsNotMapped)
            {
                distinct = " DISTINCT";
                schoolInfoColumns = ", -1 as SchoolId, '' AS SchoolName";
                schoolYearInfoColumns = ", -1 as SchoolYear, '' AS SchoolYearDescription";

                sbWhere.Append(query.SchoolQueryItem != null
                    ? " AND (SchoolId IS NULL OR SchoolId <> @SchoolId)"
                    : " AND SchoolId IS NULL");

                sbWhere.Append(query.SchoolYearQueryItem != null
                    ? " AND (SchoolYear IS NULL OR SchoolYear <> @SchoolYear)"
                    : " AND SchoolYear IS NULL");
            }
            else
            {
                if (query.SchoolQueryItem != null)
                {
                    sbWhere.Append(" AND SchoolId = @SchoolId");
                }
                else
                {
                    sbWhere.Append(" AND SchoolId IS NOT NULL");
                    distinct = " DISTINCT";
                    schoolInfoColumns = ", -1 as SchoolId, '' AS SchoolName";
                }

                if (query.SchoolYearQueryItem != null)
                {
                    sbWhere.Append(" AND SchoolYear = @SchoolYear");
                }
                else
                {
                    sbWhere.Append(" AND SchoolYear IS NOT NULL");

                    distinct = " DISTINCT";
                    schoolYearInfoColumns = ", -1 as SchoolYear, '' AS SchoolYearDescription";
                }
            }

            string sql = string.Format(@"SELECT{0} ", distinct) +
                                        "CourseId, CourseNumber, Credits, " +
                                        "CourseName, CourseTypeId, " +
                                        "CourseTypeName, CourseLevelId, " +
                                        "CourseLevelName, ScoringMethodId, " +
                                        "ScoringMethodName, CourseResultGroupId, " +
                                        "CourseResultGroupName, StandardCourseId, " +
                                        "StandardCourseName, InstanceId, " +
                                        "InstanceName, ListOfSchoolNames" +
                         string.Format("{0}{1} FROM CourseView {2}", schoolInfoColumns, schoolYearInfoColumns,
                          sbWhere.Length > 0 ? "WHERE" + sbWhere : "");

            return sql;

        }
    }
}
