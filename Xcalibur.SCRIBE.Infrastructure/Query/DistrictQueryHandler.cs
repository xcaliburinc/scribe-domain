﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Domain;
using Xcalibur.SCRIBE.Core.Settings;


namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class DistrictQueryHandler : BaseQueryHandler, IQueryHandler<SearchDistrictQuery, DistrictDto>
    {
        public DistrictQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings) { }

        public IEnumerable<DistrictDto> Retrieve(SearchDistrictQuery query)
        {
            return GetList<DistrictDto>(GenerateSql(query), query);
        }

        private static string GenerateSql(SearchDistrictQuery query)
        {
            //string sql = string.Format(@"SELECT * FROM District {0} {1}", whereClause, sortOrder);
            const string sqlTemplate =
                @"SELECT * FROM District WHERE 1=1 ";

            var sbWhere = new StringBuilder("AND InstanceId = @InstanceId ");

            if (!string.IsNullOrEmpty(query.DistrictNumber))
                sbWhere.Append("AND DistrictNumber LIKE '%' + @DistrictNumber + '%' ");

            if (!string.IsNullOrEmpty(query.DistrictName))
                sbWhere.Append("AND DistrictNumber LIKE '%' + @DistrictName + '%' ");

            // NOTE:
            // ?. operator will return null if a child member is null.
            // But if we try to get a non-Nullable member, like the  Any() method,
            // that returns bool the compiler will "wrap" a return value in Nullable<>.
            // For example, Object?.Any() will give us bool? (which is Nullable<bool>), not bool.
            // Since it can't be implicitly casted to bool this expression cannot be used in the if
            if (query.UserId.HasValue && (query.Permissions?.Any() ?? false) )
            {
                sbWhere.AppendFormat(@"AND (InstanceId IN (
                                                SELECT EntityId 
                                                FROM UserToRoleToEntity INNER JOIN 
                                                    RoleToPermission ON UserToRoleToEntity.RoleId = RoleToPermission.RoleId
                                                WHERE UserId = @UserId AND EntityTypeId={0} AND EntityId = @InstanceId AND PermissionId IN @Permissions)
                                                OR
                                                DistrictId IN (
                                                SELECT EntityId 
                                                FROM UserToRoleToEntity INNER JOIN 
                                                    RoleToPermission ON UserToRoleToEntity.RoleId = RoleToPermission.RoleId
                                                WHERE UserId = @UserId AND EntityTypeId={1} AND PermissionId IN @Permissions)
                                                OR 
                                                DistrictId IN (
                                                SELECT School.DistrictId 
                                                FROM dbo.UserToRoleToEntity INNER JOIN 
                                                    RoleToPermission ON UserToRoleToEntity.RoleId = RoleToPermission.RoleId INNER JOIN 
                                                    School ON School.SchoolId = UserToRoleToEntity.EntityId
                                                WHERE UserId = @UserId AND EntityTypeId={2} AND PermissionId IN @Permissions AND InstanceId = @InstanceId) 
                                                ) ", (int)EntityType.enmEntityTypeDB.Instance, (int)EntityType.enmEntityTypeDB.District, (int)EntityType.enmEntityTypeDB.School);
            }



            return sqlTemplate + sbWhere;
        }
    }
}
