﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class BaseQueryHandler : IDisposable
    {
        #region Fields
        private IDatabaseSettings _databaseSettings;  //CDO: Is this needed?
        #endregion Fields

        #region Properties
        protected IDbConnection Connection { get; set; }
        #endregion Properties

        #region Constructors

        #region Public Methods
        public IEnumerable<T> GetList<T>(string sql, object param)
        {
            return Connection.Query<T>(sql, param);
        }
        #endregion Public Methods

        public BaseQueryHandler(IDatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;

            Connection = new SqlConnection(databaseSettings.QueryConnectionString);
            Connection.Open();
        }
        #endregion Constructors

        #region IDisposable Support

        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Free any other managed objects here.
                    if (Connection != null)
                    {
                        if (Connection.State != ConnectionState.Closed)
                            Connection.Close();
                        Connection.Dispose();
                    }
                }
                // Free any unmanaged objects here. (If include and unmanaged objects - override finalizer below)
                _disposed = true;
            }
        }

        // ~BaseQueryHandler() {
        //   Dispose(false);
        // }

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization. --
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}
