﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class UserPermissionQueryHandler : BaseQueryHandler, IQueryHandler<SearchUserPermissionQuery, UserPermissionDto>
    {
        public UserPermissionQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings)
        {
        }

        public IEnumerable<UserPermissionDto> Retrieve(SearchUserPermissionQuery query)
        {

            const string sql = @"SELECT dbo.UserToRoleToEntity.EntityId, 
    dbo.EntityType.EntityTypeId, 
    dbo.Permission.PermissionId, 
    CASE WHEN schoolEntity.InstanceId IS NOT NULL 
    THEN schoolEntity.InstanceId 
    WHEN districtEntity.InstanceId IS NOT NULL THEN districtEntity.InstanceId 
    WHEN instanceEntity.InstanceId IS NOT NULL THEN instanceEntity.InstanceId 
    ELSE - 1 END AS InstanceId,
    schoolEntity.DistrictId
FROM dbo.Role INNER JOIN
    dbo.UserToRoleToEntity INNER JOIN
    dbo.EntityType ON dbo.UserToRoleToEntity.EntityTypeId = dbo.EntityType.EntityTypeId ON dbo.Role.RoleId = dbo.UserToRoleToEntity.RoleId INNER JOIN
    dbo.RoleToPermission ON dbo.Role.RoleId = dbo.RoleToPermission.RoleId INNER JOIN
    dbo.PermissionCategory INNER JOIN
    dbo.Permission ON dbo.PermissionCategory.PermissionCategoryId = dbo.Permission.PermissionCategoryId ON dbo.RoleToPermission.PermissionId = dbo.Permission.PermissionId LEFT OUTER JOIN
    (SELECT        4 AS EntityTypeId, SchoolId AS EntityId, DistrictId, InstanceId
    FROM            dbo.School) AS schoolEntity ON schoolEntity.EntityTypeId = dbo.UserToRoleToEntity.EntityTypeId AND schoolEntity.EntityId = dbo.UserToRoleToEntity.EntityId LEFT OUTER JOIN
    (SELECT        3 AS EntityTypeId, DistrictId AS EntityId, InstanceId
    FROM            dbo.District) AS districtEntity ON districtEntity.EntityTypeId = dbo.UserToRoleToEntity.EntityTypeId AND districtEntity.EntityId = dbo.UserToRoleToEntity.EntityId LEFT OUTER JOIN
    (SELECT        2 AS EntityTypeId, InstanceId AS EntityId, InstanceId
    FROM            dbo.Instance) AS instanceEntity ON instanceEntity.EntityTypeId = dbo.UserToRoleToEntity.EntityTypeId AND instanceEntity.EntityId = dbo.UserToRoleToEntity.EntityId
WHERE UserId = @UserId
";
            return GetList<UserPermissionDto>(sql, query);
        }
    }
}
