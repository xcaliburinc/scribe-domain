﻿using System.Collections.Generic;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class UserLoginQueryHandler : BaseQueryHandler, IQueryHandler<SearchUserLoginQuery, UserLoginDto>
    {
        public UserLoginQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings) { }

        public IEnumerable<UserLoginDto> Retrieve(SearchUserLoginQuery query)
        {
            return GetList<UserLoginDto>(GenerateSql(query), query);
        }

        private static string GenerateSql(SearchUserLoginQuery query)
        {
            const string sqlTemplate = 
@"SELECT u.UserId, u.Password, u.Salt, u.IsLocked, u.IsActive, u.EmailAddress, u.IsRequiredToChangePassword, 1 as IsPasswordValid 
FROM [User] u 
WHERE u.IsActive = 1 
    AND u.Username = @Username ";

            return sqlTemplate;
        }
    }
}
