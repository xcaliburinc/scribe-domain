﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Domain;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class SchoolQueryHandler : BaseQueryHandler, IQueryHandler<SearchSchoolQuery, SchoolDto>
    {
        public SchoolQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings)
        {
        }

        public IEnumerable<SchoolDto> Retrieve(SearchSchoolQuery query)
        {
            return GetList<SchoolDto>(GenerateSql(query), query);
        }

        private static string GenerateSql(SearchSchoolQuery query)
        {
            //string sql = string.Format(@"SELECT * FROM School {0} {1}", whereClause, sortOrder);
            const string sqlTemplate =
                @"SELECT * FROM School WHERE 1=1 ";

            var sbWhere = new StringBuilder("AND InstanceId = @InstanceId ");

            if (!string.IsNullOrEmpty(query.SchoolNumber))
                sbWhere.Append("AND SchoolNumber LIKE '%' + @SchoolNumber + '%' ");

            if (!string.IsNullOrEmpty(query.SchoolName))
                sbWhere.Append("AND SchoolName LIKE '%' + @SchoolName + '%' ");

            if (query.DistrictQueryItem != null)
                sbWhere.Append("AND DistrictId = @DistrictId ");

            if (query.SchoolYearQueryItem != null)
                sbWhere.Append(
                    "AND SchoolId IN (SELECT SchoolId FROM SchoolYearDetails WHERE SchoolYear = @SchoolYear)");

            // NOTE:
            // ?. operator will return null if a child member is null.
            // But if we try to get a non-Nullable member, like the  Any() method,
            // that returns bool the compiler will "wrap" a return value in Nullable<>.
            // For example, Object?.Any() will give us bool? (which is Nullable<bool>), not bool.
            // Since it can't be implicitly casted to bool this expression cannot be used in the if
            if (query.UserId.HasValue && (query.Permissions?.Any() ?? false))
            {            
                sbWhere.AppendFormat(@"AND (InstanceId IN (
                                                SELECT EntityId 
                                                FROM UserToRoleToEntity INNER JOIN 
                                                    RoleToPermission ON UserToRoleToEntity.RoleId = RoleToPermission.RoleId
                                                WHERE UserId = @UserId AND EntityTypeId={0} AND EntityId = @InstanceId AND PermissionId IN @Permissions)
                                                OR
                                                DistrictId IN (
                                                SELECT EntityId 
                                                FROM UserToRoleToEntity INNER JOIN 
                                                    RoleToPermission ON UserToRoleToEntity.RoleId = RoleToPermission.RoleId
                                                WHERE UserId = @UserId AND EntityTypeId={1} AND PermissionId IN @Permissions)
                                                OR 
                                                SchoolId IN (
                                                SELECT EntityId 
                                                FROM UserToRoleToEntity INNER JOIN 
                                                    RoleToPermission ON UserToRoleToEntity.RoleId = RoleToPermission.RoleId
                                                WHERE UserI= @UserId AND EntityTypeId={2} AND PermissionId IN @Permissions) 
                                                ) ", (int)EntityType.enmEntityTypeDB.Instance, (int)EntityType.enmEntityTypeDB.District, (int)EntityType.enmEntityTypeDB.School);
            }

            return sqlTemplate + sbWhere;

        }
    }
}
