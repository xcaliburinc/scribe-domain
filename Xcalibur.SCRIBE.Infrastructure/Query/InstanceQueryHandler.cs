﻿using System.Collections.Generic;
using System.Text;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class InstanceQueryHandler : BaseQueryHandler, IQueryHandler<SearchInstanceQuery, InstanceDto>
    {
        public InstanceQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings)
        {
        }

        public IEnumerable<InstanceDto> Retrieve(SearchInstanceQuery query)
        {
            return GetList<InstanceDto>(GenerateSql(query), query);
        }

        private string GenerateSql(SearchInstanceQuery query)
        {
            const string sql = @"SELECT Instance.InstanceId, 
    Instance.InstanceName, 
    Instance.PRAwardNumber, 
    Instance.Address1, 
    Instance.Address2, 
    Instance.City, 
    Instance.StateId, 
    State.StateAbbreviation,
    State.StateName,
    Instance.ZipCode, 
    Instance.ProjectDirectorName, 
    Instance.ProjectDirectorPhone, 
    Instance.ProjectDirectorFax, 
    Instance.ProjectDirectorEmailAddress, 
    Instance.CertifyingOfficialName, 
    Instance.CertifyingOfficialPhone, 
    Instance.CertifyingOfficialEmailAddress, 
    Instance.PassingPercentage, 
    Instance.ExceedExpectationsPercentage, 
    Instance.DataIsImported, 
    Instance.IsActive, 
    Instance.DateCreated, 
    Instance.CreationMethodId, 
    Instance.CreatedByUserId, 
    Instance.DateLastUpdated, 
    Instance.LastUpdateMethodId, 
    Instance.LastUpdatedByUserId, 
    Instance.IsRequiredToCertifyService, 
    Instance.ServiceCertificationText, 
    Instance.IsServiceSummaryRequired, 
    Instance.CustomStudentIdLabel, 
    Instance.ShowServiceCategories, 
    Instance.ShowServiceFundingModule, 
    Instance.StudentReleaseFormLabel, 
    Instance.CustomCreditsLabel, 
    Instance.SurveyStudentIdMatchOptionsId, 
    smo.SearchMatchOptionName AS SurveyStudentIdMatchOptionsName,
    Instance.ClientPortalActive, 
    Instance.ClientPortalSurveyActive, 
    Instance.SurveyStudentIdMatchMinimalCharacters, 
    Instance.IsSurveyStudentIdRequired, 
    Instance.SurveyIdVerificationCustomStudentInstructions, 
    Instance.SurveyIdVerificationCustomParentInstructions, 
    Instance.SurveyIdVerificationCustomTeacherInstructions, 
    Instance.ClientPortalClientHomeSiteName, 
    Instance.ClientPortalClientHomeSiteURL, 
    Instance.ShowSSNField, 
    Instance.IsSchoolStudentIdRequired, 
    Instance.IsDocumentUploadActive, 
    Instance.AttachmentTierId, 
    at.AttachmentTierName,
    Instance.AdditionalSpaceAllocatedKBs, 
    Instance.DocumentUploadSpaceUsedKBs, 
    Instance.CustomStudentIdLabelSpanish, 
    Instance.IsSurveyBirthdateRequired, 
    Instance.SurveyIdVerificationCustomStudentInstructionsSpanish, 
    Instance.SurveyIdVerificationCustomParentInstructionsSpanish, 
    Instance.SurveyIdVerificationCustomTeacherInstructionsSpanish, 
    Instance.ClientPortalClientHomeSiteNameSpanish, 
    Instance.ClientPortalAllowSpanish, 
    Instance.IsGeocodingActive, 
    Instance.ShowSystemServiceSummaries, 
    Instance.IsActEngageTracked, 
    Instance.ShowCollegeEnrollmentModule, 
    Instance.AllowAddingCollegeEnrollment, 
    Instance.NSCImportFormatId, 
    Instance.NumberSCRIBEUserLicenses, 
    Instance.IsElementaryImplementation, 
    Instance.IsHighSchoolImplementation, 
    Instance.IsUnderGraduateImplementation, 
    Instance.IsPostDoctorateImplementation, 
    Instance.IsMastersImplementation, 
    Instance.ShowMatchTrackingModule, 
    Instance.IndirectCostPercentage, 
    Instance.MileageReimbursement, 
    Instance.CCRECStateId, 
    Instance.CCRECServiceAggreementNumber, 
    Instance.CustomSchoolIdLabel, 
    Instance.CustomSchoolObjectName, 
    Instance.CustomSchoolObjectNamePluralForm, 
    Instance.CustomDistrictIdLabel, 
    Instance.CustomDistrictObjectName, 
    Instance.CustomDistrictObjectNamePluralForm, 
    Instance.ShowExpenseReportModule, 
    Instance.ExpenseReportRejectionStatusCustomName, 
    Instance.ExpenseReportApprovalStatusCustomName, 
    Instance.ExpenseReportDraftStatusCustomName, 
    Instance.NSCAccountNumber, 
    Instance.NSCOrganizationName
FROM Instance
    LEFT JOIN dbo.State ON State.StateId = dbo.Instance.StateId
    LEFT JOIN dbo.SearchMatchOptions smo ON smo.SearchMatchOptionId = dbo.Instance.SurveyStudentIdMatchOptionsId
    LEFT JOIN dbo.AttachmentTier at ON at.AttachmentTierId = dbo.Instance.AttachmentTierId ";

            StringBuilder sbWhere = new StringBuilder();

            if (query.InstanceId.HasValue)
                sbWhere.Append("Instance.InstanceId = @InstanceId AND ");
            if (query.IsActive.HasValue)
                sbWhere.Append("Instance.IsActive = @IsActive AND ");

            if (sbWhere.Length > 0)
            {
                sbWhere.Length -= 4;
                sbWhere.Insert(0, "WHERE ");
            }

            return sql + sbWhere;
        }
    }
}
