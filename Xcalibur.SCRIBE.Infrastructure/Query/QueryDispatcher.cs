﻿using System.Collections.Generic;
using Autofac;
using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private ILifetimeScope _scope;

        public QueryDispatcher(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public IEnumerable<TResult> Dispatch<TParameter, TResult>(TParameter query)
            where TParameter : IQuery
            where TResult : IQueryResult
        {
            using (IQueryHandler<TParameter, TResult> handler =
                _scope.Resolve<IQueryHandler<TParameter, TResult>>())
            {
                return handler.Retrieve(query);
            }
        }
    }
}
