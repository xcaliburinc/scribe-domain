﻿using System.Collections.Generic;
using System.Text;
using Xcalibur.SCRIBE.Core.DataTransferObjects;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Settings;

namespace Xcalibur.SCRIBE.Infrastructure.Query
{
    public class UserInstanceQueryHandler  : BaseQueryHandler, IQueryHandler<SearchUserQuery, UserInstanceDto>
    {
        public UserInstanceQueryHandler(IDatabaseSettings databaseSettings) : base(databaseSettings) { }

        public IEnumerable<UserInstanceDto> Retrieve(SearchUserQuery query)
        {
            return GetList<UserInstanceDto>(GenerateSql(query), query);
        }

        private static string GenerateSql(SearchUserQuery query)
        {
            string instanceInfoColumns = query.InstanceId != null ? 
                " uti.IsDefaultInstance, uti.InstanceId, i.InstanceName, " : 
                " 0 IsDefaultInstance , -1 as InstanceId, '' InstanceName, ";
            string distinct = query.InstanceId != null ? "" : "Distinct";
            string sqlTemplate = string.Format(
@"SELECT {0} {1}
	u.UserId,
	u.FirstName, 
	u.MiddleName, 
	u.LastName, 
	u.EmailAddress, 
	u.UserName, 
	u.IsRequiredToChangePassword, 
	u.IsActive, 
	u.IsLocked, 
	u.DateLocked,
	u.ReasonLocked,
	u.DateCreated, 
	u.CreationMethodId,
	qCreationModificationMethod.ModificationMethodName AS CreationMethodName, 
	u.CreatedByUserId, 
	CASE WHEN qCreatedByUser.UserId IS NOT NULL THEN qCreatedByUser.FirstName + ' ' + qCreatedByUser.LastName ELSE NULL END AS CreatedByUserName,
	u.DateLastUpdated, 
	u.LastUpdateMethodId,
	qUpdateModificationMethod.ModificationMethodName AS LastUpdateMethodName,
	u.LastUpdatedByUserId,
	CASE WHEN qUpdatedByUser.UserId IS NOT NULL THEN qUpdatedByUser.FirstName + ' ' + qUpdatedByUser.LastName ELSE NULL END AS UpdatedByUserName, 
	u.IPAddressLastLogin, 
	u.DateLastLogin, 
	u.CanRequestPassword, 
	u.WorkPhone, 
	u.MobilePhone,
    CAST(CASE WHEN UsersWithSystemAdminPermission.UserId IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS UserHasSystemAdminPermission 
FROM UserToInstance uti 
	INNER JOIN Instance i ON uti.InstanceId = i.InstanceId 
	RIGHT OUTER JOIN [User] u ON uti.UserId = u.UserId
    LEFT JOIN                           
		(SELECT DISTINCT UserId
			FROM UserToRoleToEntity 
			INNER JOIN Role ON UserToRoleToEntity.RoleId = dbo.Role.RoleId 
			INNER JOIN dbo.RoleToPermission ON dbo.Role.RoleId = dbo.RoleToPermission.RoleId
		WHERE PermissionId = 76) UsersWithSystemAdminPermission ON u.UserId = UsersWithSystemAdminPermission.UserId
	LEFT OUTER JOIN (
		SELECT u.UserId, u.FirstName, u.LastName
		FROM [User] u) qCreatedByUser ON qCreatedByUser.UserId = u.CreatedByUserId
	LEFT OUTER JOIN (
		SELECT u.UserId, u.FirstName, u.LastName
		FROM [User] u) qUpdatedByUser ON qUpdatedByUser.UserId = u.LastUpdatedByUserId
	LEFT OUTER JOIN (
		SELECT ModificationMethod.ModificationMethodId, ModificationMethod.ModificationMethodName 
		FROM ModificationMethod) qCreationModificationMethod ON qCreationModificationMethod.ModificationMethodId = u.CreationMethodId
	LEFT OUTER JOIN (
		SELECT ModificationMethod.ModificationMethodId, ModificationMethod.ModificationMethodName 
		FROM ModificationMethod) qUpdateModificationMethod ON qUpdateModificationMethod.ModificationMethodId = u.CreationMethodId
WHERE 1 = 1 ", distinct, instanceInfoColumns);
          
            var sbWhere = new StringBuilder();
            if (query.InstanceId.HasValue)
                sbWhere.Append("AND u.InstanceId = @InstanceId ");
            else if (!string.IsNullOrEmpty(query.InstanceIds)) 
                sbWhere.Append("AND (u.InstanceId is NULL OR u.InstanceId in (@InstanceIds)) ");
            if (query.UserId.HasValue)
                sbWhere.Append("AND u.UserId = @UserId ");
            if (!string.IsNullOrEmpty(query.UserIdsToExclude))
                sbWhere.Append("AND u.UserId IN (@UserIdsToExclude) ");
            if (!string.IsNullOrEmpty(query.Username))
                sbWhere.Append("AND u.Username = @Username ");
            if (!string.IsNullOrEmpty(query.FirstName))
                sbWhere.Append("AND u.FirstName LIKE '%' + @FirstName + '%' ");
            if (!string.IsNullOrEmpty(query.LastName))
                sbWhere.Append("AND u.LastName LIKE '%' + @LastName + '%' ");
            if (!string.IsNullOrEmpty(query.WorkPhone))
                sbWhere.Append("AND u.WorkPhone LIKE '%' + @WorkPhone + '%' ");
            if (!string.IsNullOrEmpty(query.MobilePhone))
                sbWhere.Append("AND u.MobilePhone LIKE '%' + @MobilePhone + '%' ");
            if (!string.IsNullOrEmpty(query.EmailAddress))
                sbWhere.Append("AND u.EmailAddress LIKE '%' + @EmailAddress + '%' ");
            if (query.IsActive.HasValue)
                sbWhere.Append("AND u.IsActive = @IsActive ");
            if (query.IsLocked.HasValue)
                sbWhere.Append("AND u.IsLocked = @IsLocked ");
            if (query.IsRequiredToChangePassword.HasValue)
                sbWhere.Append("AND u.IsRequiredToChangePassword = @IsRequiredToChangePassword ");
            if (query.DateLockedEnd.HasValue)
                sbWhere.Append("AND u.DateLocked < DATEADD(d,1,@DateLockedEnd) ");
            if (query.DateLockedStart.HasValue)
                sbWhere.Append("AND u.DateLocked >= @DateLockedStart ");
            if (query.DateLastLoggedInEnd.HasValue)
                sbWhere.Append("AND u.DateLastLoggedInEnd < DATEADD(d,1,@DateLastLoggedInEnd) ");
            if (query.DateLastLoggedInStart.HasValue)
                sbWhere.Append("AND u.DateLastLoggedInStart >= @DateLastLoggedInStart ");

            return sqlTemplate + sbWhere;
        }
    }
}
