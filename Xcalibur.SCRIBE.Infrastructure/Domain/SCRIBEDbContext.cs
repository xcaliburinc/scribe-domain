﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xcalibur.SCRIBE.Core.Settings;
using Xcalibur.SCRIBE.Core.Domain;

namespace Xcalibur.SCRIBE.Infrastructure.Domain
{
    public class SCRIBEDbContext : DbContext
    {
        static SCRIBEDbContext()
        {
            Database.SetInitializer<SCRIBEDbContext>(null);
        }

        public SCRIBEDbContext(IDatabaseSettings databaseSettings) : base(databaseSettings.CommandConnectionString)
        {
            Database.SetInitializer<SCRIBEDbContext>(null);
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
        }

        public bool HasUnsavedChanges()
        {
            return ChangeTracker.HasChanges();
        }

        /// <summary>
        /// Set the Date/User/Method modified fields for new or updated entities
        /// </summary>
        /// <param name="userId"></param>
        public void UpdateModificationInfo(int userId)
        {
            if (!HasUnsavedChanges())
                return;

            foreach (var item in ChangeTracker.Entries<BaseCreateUpdateDomain>())
            {
                bool isCreated = false;

                switch (item.State)
                {
                    case EntityState.Added:
                        isCreated = true;
                        break;
                    case EntityState.Modified:
                        break;

                    case EntityState.Deleted:
                    case EntityState.Detached:
                    case EntityState.Unchanged:
                        // Don't update Modified info for these states
                        continue;
                }

                item.Entity.UpdateModificationInfo(isCreated, userId);
            }
        }

    }
}
