﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcalibur.SCRIBE.Client
{
    public static class Constants
    {
        /// <summary>
        /// CSS for Alert Danger
        /// </summary>
        public static readonly string CSS_ALERT_DANGER = "alert alert-danger";

        /// <summary>
        /// CSS for Alert Info
        /// </summary>
        public static readonly string CSS_ALERT_INFO = "alert alert-info";

        /// <summary>
        /// CSS for Alert Success
        /// </summary>
        public static readonly string CSS_ALERT_SUCCESS = "alert alert-success";

        /// <summary>
        /// CSS for Alert Warning
        /// </summary>
        public static readonly string CSS_ALERT_WARNING = "alert alert-warning";
    }
}
