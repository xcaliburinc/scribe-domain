﻿using Xcalibur.SCRIBE.Core.Settings;
using System.Configuration;

namespace Xcalibur.SCRIBE.Client.Configs
{
    public class SCRIBESettings : ICryptographySettings, IDatabaseSettings, IAppSettings
    {
        private const string CONNECTION_STRING = "Xcalibur.SCRIBE.DomainLogic.DataManager";
        private const string SCRIBE_VERSION = "SCRIBEVersion";
        private const string MAX_FAILED_LOGIN_ATTEMPTS_ALLOWED = "FailedAttemptsUntilLocked";

        public string EncryptionKey { get; private set; }
        public string CommandConnectionString { get; private set; }
        public string QueryConnectionString { get; private set; }
        public string SCRIBEVersion { get; private set; }
        public int MaxFailedLoginAttemptsAllowed { get; private set; }

        public SCRIBESettings()
        {
            QueryConnectionString = ConfigurationManager.ConnectionStrings[CONNECTION_STRING].ConnectionString;
            CommandConnectionString = ConfigurationManager.ConnectionStrings[CONNECTION_STRING].ConnectionString;
            SCRIBEVersion = ConfigurationManager.AppSettings[SCRIBE_VERSION];
            MaxFailedLoginAttemptsAllowed = int.Parse(ConfigurationManager.AppSettings[MAX_FAILED_LOGIN_ATTEMPTS_ALLOWED]);
        }
    }
}
