﻿using System;
using System.Web;
using Xcalibur.SCRIBE.Core.DataTransferObjects;

namespace Xcalibur.SCRIBE.Client
{
    public class SessionHandler
    {
        #region Static Fields and Constants

        private const string SESSION_KEY_USER = "CurrentUser";
        private const string SESSION_KEY_CURRENT_INSTANCE = "CurrentInstance";

        #endregion

        #region Fields

        private HttpSessionStateBase _session;

        #endregion

        #region Constructors

        public SessionHandler(HttpSessionStateBase session)
        {
            _session = session;
        }

        #endregion

        #region Public Methods

        public void ClearSession()
        {
            _session.Clear();
        }

        public UserWithPermissionsDto GetUser()
        {
            if (_session[SESSION_KEY_USER] == null)
                return null;
            return _session[SESSION_KEY_USER] as UserWithPermissionsDto;
        }

        public UserWithPermissionsDto GetUserAndConfirmUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                return null;
            var user = GetUser();
            if (user == null)
                return null;
            if (user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase))
                return user;
            RemoveUser();
            return null;
        }

        public void RemoveSessionData()
        {
            RemoveInstance();
            RemoveUser();
        }

        public void RemoveUser()
        {
            _session.Remove(SESSION_KEY_USER);
        }

        public void SaveUser(UserWithPermissionsDto user)
        {
            _session[SESSION_KEY_USER] = user;
        }

        public void SaveInstance(InstanceDto instance)
        {
            _session[SESSION_KEY_CURRENT_INSTANCE] = instance;
        }

        public InstanceDto GetInstance()
        {
            return _session[SESSION_KEY_CURRENT_INSTANCE] as InstanceDto;
        }
        public void RemoveInstance()
        {
            _session.Remove(SESSION_KEY_CURRENT_INSTANCE);
        }


        #endregion
    }
}
