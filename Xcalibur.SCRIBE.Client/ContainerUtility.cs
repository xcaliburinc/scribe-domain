﻿using Autofac;
using Xcalibur.SCRIBE.Client.Configs;
using Xcalibur.SCRIBE.Core.Query;
using Xcalibur.SCRIBE.Core.Services;
using Xcalibur.SCRIBE.Core.Settings;
using Xcalibur.SCRIBE.Infrastructure.Query;
using Xcalibur.SCRIBE.Infrastructure.Services;

namespace Xcalibur.SCRIBE.Client
{
    public static class ContainerUtility
    {
        public static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();

            RegisterItems(builder);

            return builder.Build();
        }

        public static void RegisterItems(ContainerBuilder builder)
        {
            builder.RegisterType<SCRIBESettings>().As<IDatabaseSettings>().As<ICryptographySettings>().As<IAppSettings>().SingleInstance();

            var asm = typeof(BaseQueryHandler).Assembly;
            builder.RegisterAssemblyTypes(asm)
                .Where(t => t.Name.EndsWith("QueryHandler"))
                .AsImplementedInterfaces();

            builder.RegisterType<QueryDispatcher>().As<IQueryDispatcher>();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<InstanceService>().As<IInstanceService>();
        }
    }
}
