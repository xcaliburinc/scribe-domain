﻿using System;

namespace Xcalibur.SCRIBE.Client
{
    public class Alert
    {
        /// <summary>
        /// Type of Alert
        /// </summary>
        public enum enmAlertType
        {
            Success = 1,
            Info = 2,
            Warning = 3,
            Danger = 4
        }
        
        /// <summary>
        /// Creates an instance of an Alert object
        /// </summary>
        /// <param name="message">Message to display</param>
        /// <param name="alertType">Type of alert</param>
        public Alert(string message, enmAlertType alertType)
        {
            Message = message;
            AlertType = alertType;
        }

        /// <summary>
        /// Set or get Type of Alert
        /// </summary>
        public enmAlertType? AlertType { get; set; }

        /// <summary>
        /// Get CSS Class
        /// </summary>
        public string CSSClass
        {
            get
            {
                if (AlertType == null)
                    return "";
                switch (AlertType)
                {
                    case enmAlertType.Success:
                        return Constants.CSS_ALERT_SUCCESS;
                    case enmAlertType.Info:
                        return Constants.CSS_ALERT_INFO;
                    case enmAlertType.Warning:
                        return Constants.CSS_ALERT_WARNING;
                    case enmAlertType.Danger:
                        return Constants.CSS_ALERT_DANGER;
                    default:
                        throw new ArgumentOutOfRangeException(AlertType.ToString());
                }
            }
        }

        /// <summary>
        /// Set or get Message Text
        /// </summary>
        public string Message { get; set; }
    }
}
