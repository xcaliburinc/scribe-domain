﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Core.Test.Query
{
    [TestClass]
    public class SearchDistrictQueryTests
    {
        #region Public Methods

        [TestMethod]
        public void IsCriteriaEmptyAllCriteriaSet()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyDistrictName()
        {
            var query = GetSearchDistrictQueryNoCriteriaSet();
            query.DistrictName = "DistrictName";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyDistrictNumber()
        {
            var query = GetSearchDistrictQueryNoCriteriaSet();
            query.DistrictNumber = "1234";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceAllCriteriaSet()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        public void IsCriteriaEmptyExceptInstanceDistrictName()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            query.DistrictName = "DistrictName";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceDistrictNumber()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            query.DistrictNumber = "1234";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void ToCriteriaString()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            const string expectedCriteriaString = "SearchSessionId - S2; " +
                                                  "DistrictName - DistrictName; " +
                                                  "DistrictNumber - 1234; ";
            Assert.AreEqual(expectedCriteriaString, query.ToCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            const string expectedCriteriaString = "<strong>District Name:</strong> DistrictName; " +
                                                  "<strong>District Number:</strong> 1234; ";

            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }


        [TestMethod]
        public void ToFormattedCriteriaString_NoDistrictName()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            query.DistrictName = string.Empty;
            const string expectedCriteriaString = "<strong>District Number:</strong> 1234; ";

            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_NoDistrictNumber()
        {
            var query = GetSearchDistrictQueryAllCriteriaSet();
            query.DistrictNumber = string.Empty;
            const string expectedCriteriaString = "<strong>District Name:</strong> DistrictName; ";

            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get SearchDistrictQuery with all criteria properties set
        /// </summary>
        /// <returns>SearchDistrictQuery</returns>
        private SearchDistrictQuery GetSearchDistrictQueryAllCriteriaSet()
        {
            var query = new SearchDistrictQuery
            {
                DistrictName = "DistrictName",
                DistrictNumber = "1234",
                InstanceId = 1,
                SearchSessionId = "S2"
            };
            return query;
        }

        /// <summary>
        /// Get SearchDistrictQuery with only InstanceId criteria properties set
        /// </summary>
        /// <returns>SearchDistrictQuery</returns>
        private SearchDistrictQuery GetSearchDistrictQueryInstanceOnlySet()
        {
            var query = new SearchDistrictQuery()
            {
                InstanceId = 1
            };
            return query;
        }

        /// <summary>
        /// Get SearchDistrictQuery with no criteria properties set
        /// </summary>
        /// <returns>SearchDistrictQuery</returns>
        private SearchDistrictQuery GetSearchDistrictQueryNoCriteriaSet()
        {
            return new SearchDistrictQuery();
        }

        #endregion
    }
}
