﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Xcalibur.SCRIBE.Core.Query.Test
{
    [TestClass]
    public class SearchCourseQueryTests
    {
        #region Public Methods

        [TestMethod]
        public void IsCriteriaEmptyAllCriteriaSet()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyCourseLevelQueryItem()
        {
            var query = GetSearchCourseQueryNoCriteriaSet();
            query.CourseLevelQueryItem.Value = "1";
            query.CourseLevelQueryItem.Text = "CourseLevel";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyCourseName()
        {
            var query = GetSearchCourseQueryNoCriteriaSet();
            query.CourseName = "CourseName";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.CourseName = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyCourseNumber()
        {
            var query = GetSearchCourseQueryNoCriteriaSet();
            query.CourseNumber = "CourseNumber123";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.CourseNumber = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyCourseResultGroupQueryItem()
        {
            var query = GetSearchCourseQueryNoCriteriaSet();
            query.CourseResultGroupQueryItem.Value = "1";
            query.CourseResultGroupQueryItem.Text = "CourseResultGroup";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyCourseTypeQueryItem()
        {
            var query = GetSearchCourseQueryNoCriteriaSet();
            query.CourseTypeQueryItem.Value = "1";
            query.CourseTypeQueryItem.Text = "CourseType";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }



        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceAllCriteriaSet()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceCourseLevelQueryItem()
        {
            var query = GetSearchCourseQueryInstanceOnlySet();
            query.CourseLevelQueryItem.Value = "1";
            query.CourseLevelQueryItem.Text = "CourseLevel";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceCourseName()
        {
            var query = GetSearchCourseQueryInstanceOnlySet();
            query.CourseName = "CourseName";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.CourseName = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceCourseNumber()
        {
            var query = GetSearchCourseQueryInstanceOnlySet();
            query.CourseNumber = "CourseNumber123";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.CourseNumber = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceCourseResultGroupQueryItem()
        {
            var query = GetSearchCourseQueryInstanceOnlySet();
            query.CourseResultGroupQueryItem.Value = "1";
            query.CourseResultGroupQueryItem.Text = "CourseResultGroup";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceCourseTypeQueryItem()
        {
            var query = GetSearchCourseQueryInstanceOnlySet();
            query.CourseTypeQueryItem.Value = "1";
            query.CourseTypeQueryItem.Text = "CourseType";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }


        [TestMethod]
        public void IsCriteriaEmptyNoCriteriaSet()
        {
            var query = GetSearchCourseQueryNoCriteriaSet();
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptySearchSessionId()
        {
            var query = GetSearchCourseQueryNoCriteriaSet();
            query.SearchSessionId = "Test";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.SearchSessionId = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }



        [TestMethod]
        public void ToCriteriaString()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            //CourseLevelQueryItem = new QueryItem("1", "CourseLevel"),
            //CourseName = "CourseName",
            //CourseNumber = "CourseNumber",
            //CourseResultGroupQueryItem = new QueryItem("1", "CourseResultGroup"),
            //CourseTypeQueryItem = new QueryItem("1", "CourseType"),
            //StandardCourseQueryItem = new QueryItem("1", "StandardCourse"),
            //InstanceId = 1,
            //IsNotMapped = false,
            //SearchSessionId = "C1"
            const string expectedCriteriaString = "SearchSessionId - C1; CourseLevelValue - 1; CourseLevelText - CourseLevel; CourseName - CourseName; CourseNumber - CourseNumber; " +
                                                  "CourseResultGroupValue - 1; CourseResultGroupText - CourseResultGroup; " +
                                                  "CourseTypeValue - 1; CourseTypeText - CourseType; " +
                                                  "StandardCourseValue - 1; StandardCourseText - StandardCourse; " +
                                                  "InstanceId - 1; IsNotMapped - false; ";
            Assert.AreEqual(expectedCriteriaString, query.ToCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            const string expectedCriteriaString = "<strong>Course Level:</strong> CourseLevel; " +
                                                  "<strong>Course Name:</strong> CourseName; " + 
                                                  "<strong>Course Number:</strong> CourseNumber; " +
                                                  "<strong>Course Result Group:</strong> CourseResultGroup; " +
                                                  "<strong>Course Type:</strong> CourseType; " +
                                                  "<strong>Standard Course:</strong> StandardCourse; " +
                                                  "<strong>Is Not Mapped:</strong> no; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_NoCourseLevel()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            query.CourseLevelQueryItem = null;
            const string expectedCriteriaString = "<strong>Course Name:</strong> CourseName; " + 
                                                  "<strong>Course Number:</strong> CourseNumber; " +
                                                  "<strong>Course Result Group:</strong> CourseResultGroup; " +
                                                  "<strong>Course Type:</strong> CourseType; " +
                                                  "<strong>Standard Course:</strong> StandardCourse; " +
                                                  "<strong>Is Not Mapped:</strong> no; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }


        [TestMethod]
        public void ToFormattedCriteriaString_NoCourseName()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            query.CourseName = string.Empty;
            const string expectedCriteriaString = "<strong>Course Level:</strong> CourseLevel; " +
                                                  "<strong>Course Number:</strong> CourseNumber; " +
                                                  "<strong>Course Result Group:</strong> CourseResultGroup; " +
                                                  "<strong>Course Type:</strong> CourseType; " +
                                                  "<strong>Standard Course:</strong> StandardCourse; " +
                                                  "<strong>Is Not Mapped:</strong> no; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        public void ToFormattedCriteriaString_NoCourseNumber()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            query.CourseNumber = string.Empty;
            const string expectedCriteriaString = "<strong>Course Level:</strong> CourseLevel; " +
                                                  "<strong>Course Name:</strong> CourseName; " +
                                                  "<strong>Course Result Group:</strong> CourseResultGroup; " +
                                                  "<strong>Course Type:</strong> CourseType; " +
                                                  "<strong>Standard Course:</strong> StandardCourse; " +
                                                  "<strong>Is Not Mapped:</strong> no; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_NoCourseResultGroup()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            query.CourseResultGroupQueryItem = null;
            const string expectedCriteriaString = "<strong>Course Level:</strong> CourseLevel; " +
                                                  "<strong>Course Name:</strong> CourseName; " +
                                                  "<strong>Course Number:</strong> CourseNumber; " +
                                                  "<strong>Course Type:</strong> CourseType; " +
                                                  "<strong>Standard Course:</strong> StandardCourse; " +
                                                  "<strong>Is Not Mapped:</strong> no; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_NoCourseType()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            query.CourseTypeQueryItem = null;
            const string expectedCriteriaString = "<strong>Course Level:</strong> CourseLevel; " +
                                                  "<strong>Course Name:</strong> CourseName; " +
                                                  "<strong>Course Number:</strong> CourseNumber; " +
                                                  "<strong>Course Result Group:</strong> CourseResultGroup; " +
                                                  "<strong>Standard Course:</strong> StandardCourse; " +
                                                  "<strong>Is Not Mapped:</strong> no; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_NoStandardCourse()
        {
            var query = GetSearchCourseQueryAllCriteriaSet();
            const string expectedCriteriaString = "<strong>Course Level:</strong> CourseLevel; " +
                                                  "<strong>Course Name:</strong> CourseName; " +
                                                  "<strong>Course Number:</strong> CourseNumber; " +
                                                  "<strong>Course Result Group:</strong> CourseResultGroup; " +
                                                  "<strong>Course Type:</strong> CourseType; " +
                                                  "<strong>Is Not Mapped:</strong> no; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Get SearchCourseQuery with all criteria properties set
        /// </summary>
        /// <returns>SearchCourseQuery</returns>
        private SearchCourseQuery GetSearchCourseQueryAllCriteriaSet()
        {
            var query = new SearchCourseQuery
            {
                CourseLevelQueryItem = new QueryItem("1", "CourseLevel"),
                CourseName = "CourseName",
                CourseNumber = "CourseNumber",
                CourseResultGroupQueryItem = new QueryItem("1", "CourseResultGroup"),
                CourseTypeQueryItem = new QueryItem("1", "CourseType"),
                StandardCourseQueryItem = new QueryItem("1", "StandardCourse"),
                InstanceId = 1,
                IsNotMapped = false,
                SearchSessionId = "C1"

            };
            return query;
        }

        /// <summary>
        /// Get SearchCourseQuery with only InstanceId criteria properties set
        /// </summary>
        /// <returns>SearchCourseQuery</returns>
        private SearchCourseQuery GetSearchCourseQueryInstanceOnlySet()
        {
            var query = new SearchCourseQuery()
            {
                InstanceId = 1
            };
            return query;
        }

        /// <summary>
        /// Get SearchCourseQuery with no criteria properties set
        /// </summary>
        /// <returns>SearchCourseQuery</returns>
        private SearchCourseQuery GetSearchCourseQueryNoCriteriaSet()
        {
            return new SearchCourseQuery();
        }

        #endregion
    }
}
