﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Xcalibur.SCRIBE.Core.Query.Test
{
    [TestClass]
    public class SearchUserQueryTests
    {
        #region Public Methods

        [TestMethod]
        public void IsCriteriaEmptyAllCriteriaSet()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyDateLastLoggedInEnd()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.DateLastLoggedInEnd = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyDateLastLoggedInStart()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.DateLastLoggedInStart = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyDateLockedEnd()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.DateLockedEnd = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyDateLockedStart()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.DateLockedStart = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyEmailAddress()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.EmailAddress = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.EmailAddress = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceAllCriteriaSet()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceDateLastLoggedInEnd()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.DateLastLoggedInEnd = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceDateLastLoggedInStart()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.DateLastLoggedInStart = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceDateLockedEnd()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.DateLockedEnd = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceDateLockedStart()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.DateLockedStart = DateTime.Now;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceEmailAddress()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.EmailAddress = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.EmailAddress = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceFirstName()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.FirstName = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.FirstName = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceInstanceIds()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.InstanceIds = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.InstanceIds = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceIsActive()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.IsActive = true;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceIsLocked()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.IsLocked = true;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceIsRequiredToChangePassword()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.IsRequiredToChangePassword = true;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceLastName()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.LastName = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.LastName = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceMobilePhone()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.MobilePhone = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.MobilePhone = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceNoCriteriaSet()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceSearchSessionId()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.SearchSessionId = "Test";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.SearchSessionId = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceUserHasSystemAdminPermission()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.UserHasSystemAdminPermission = true;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceUserId()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.UserId = 1;
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceUsername()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.Username = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.Username = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceWorkPhone()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.WorkPhone = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.WorkPhone = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyFirstName()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.FirstName = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.FirstName = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyInstanceExceptUserIdsToExclude()
        {
            var query = GetSearchUserQueryInstanceOnlySet();
            query.UserIdsToExclude = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
            //Check that empty string is considered empty
            query.UserIdsToExclude = "";
            Assert.AreEqual(true, query.IsCriteriaEmptyExceptInstance);
        }

        [TestMethod]
        public void IsCriteriaEmptyInstanceId()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.InstanceId = 1;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyInstanceIds()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.InstanceIds = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.InstanceIds = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyIsActive()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.IsActive = true;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyIsLocked()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.IsLocked = true;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyIsRequiredToChangePassword()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.IsRequiredToChangePassword = true;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyLastName()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.LastName = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.LastName = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyMobilePhone()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.MobilePhone = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.MobilePhone = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyNoCriteriaSet()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptySearchSessionId()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.SearchSessionId = "Test";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.SearchSessionId = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyUserHasSystemAdminPermission()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.UserHasSystemAdminPermission = true;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyUserId()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.UserId = 1;
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyUserIdsToExclude()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.UserIdsToExclude = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.UserIdsToExclude = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyUsername()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.Username = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.Username = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyWorkPhone()
        {
            var query = GetSearchUserQueryNoCriteriaSet();
            query.WorkPhone = "Test";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
            //Check that empty string is considered empty
            query.WorkPhone = "";
            Assert.AreEqual(true, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void ToCriteriaString()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            const string expectedCriteriaString = "SearchSessionId - T1; First Name - Firstname; Last Name - Lastname; Username - Username; " +
                                                  "Work Phone - 111-111-1111; Mobile Phone - 111-111-1111; Is Active - Yes; Is Locked - No; " +
                                                  "Date Locked Start - 12/31/2018; Date Locked End - 12/31/2018; Date Last Logged In Start - 12/31/2018; " +
                                                  "Date Last Logged In End - 12/31/2018; Email Address - test@test.com; " +
                                                  "Is Required To Change Password - No; Instance Id - 1; Instance Ids - 1,2; " +
                                                  "User Id - 1; User Ids to Exclude - 1,2; User has System Admin Permission - Yes; ";
            Assert.AreEqual(expectedCriteriaString, query.ToCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            const string expectedCriteriaString = " <strong>First Name:</strong> Firstname <strong>Last Name:</strong> Lastname <strong>Username:</strong> Username " +
                                                  "<strong>Email:</strong> test@test.com <strong>Work Phone:</strong> 111-111-1111 <strong>Mobile Phone:</strong> 111-111-1111 " +
                                                  "<strong>Date Last Logged In:</strong> 12/31/2018 to 12/31/2018 <strong>Active:</strong> Yes <strong>Required To Change Password:</strong> No " +
                                                  "<strong>Locked:</strong> No <strong>Date Locked:</strong> 12/31/2018 to 12/31/2018";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_DateLastLoggedInEnd()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            query.DateLastLoggedInStart = null;
            const string expectedCriteriaString = " <strong>First Name:</strong> Firstname <strong>Last Name:</strong> Lastname <strong>Username:</strong> Username " +
                                                  "<strong>Email:</strong> test@test.com <strong>Work Phone:</strong> 111-111-1111 <strong>Mobile Phone:</strong> 111-111-1111 " +
                                                  "<strong>Date Last Logged In:</strong> On or Before 12/31/2018 <strong>Active:</strong> Yes <strong>Required To Change Password:</strong> No " +
                                                  "<strong>Locked:</strong> No <strong>Date Locked:</strong> 12/31/2018 to 12/31/2018";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_DateLastLoggedInStart()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            query.DateLastLoggedInEnd = null;
            const string expectedCriteriaString = " <strong>First Name:</strong> Firstname <strong>Last Name:</strong> Lastname <strong>Username:</strong> Username " +
                                                  "<strong>Email:</strong> test@test.com <strong>Work Phone:</strong> 111-111-1111 <strong>Mobile Phone:</strong> 111-111-1111 " +
                                                  "<strong>Date Last Logged In:</strong> On or After 12/31/2018 <strong>Active:</strong> Yes <strong>Required To Change Password:</strong> No " +
                                                  "<strong>Locked:</strong> No <strong>Date Locked:</strong> 12/31/2018 to 12/31/2018";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_DateLockedEnd()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            query.DateLockedStart = null;
            const string expectedCriteriaString = " <strong>First Name:</strong> Firstname <strong>Last Name:</strong> Lastname <strong>Username:</strong> Username " +
                                                  "<strong>Email:</strong> test@test.com <strong>Work Phone:</strong> 111-111-1111 <strong>Mobile Phone:</strong> 111-111-1111 " +
                                                  "<strong>Date Last Logged In:</strong> 12/31/2018 to 12/31/2018 <strong>Active:</strong> Yes <strong>Required To Change Password:</strong> No " +
                                                  "<strong>Locked:</strong> No <strong>Date Locked:</strong> On or Before 12/31/2018";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_DateLockedStart()
        {
            var query = GetSearchUserQueryAllCriteriaSet();
            query.DateLockedEnd = null;
            const string expectedCriteriaString = " <strong>First Name:</strong> Firstname <strong>Last Name:</strong> Lastname <strong>Username:</strong> Username " +
                                                  "<strong>Email:</strong> test@test.com <strong>Work Phone:</strong> 111-111-1111 <strong>Mobile Phone:</strong> 111-111-1111 " +
                                                  "<strong>Date Last Logged In:</strong> 12/31/2018 to 12/31/2018 <strong>Active:</strong> Yes <strong>Required To Change Password:</strong> No " +
                                                  "<strong>Locked:</strong> No <strong>Date Locked:</strong> On or After 12/31/2018";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }
        //CDO:  Add test for each field - ToFormattedCriteriaString and ToCriteriaString
        #endregion

        #region Private Methods

        /// <summary>
        /// Get SearchUserQuery with all criteria properties set
        /// </summary>
        /// <returns>SearchUserQuery</returns>
        private SearchUserQuery GetSearchUserQueryAllCriteriaSet()
        {
            var query = new SearchUserQuery
            {
                DateLastLoggedInEnd = DateTime.Parse("12/31/2018"),
                DateLastLoggedInStart = DateTime.Parse("12/31/2018"),
                DateLockedEnd = DateTime.Parse("12/31/2018"),
                DateLockedStart = DateTime.Parse("12/31/2018"),
                EmailAddress = "test@test.com",
                FirstName = "Firstname",
                InstanceId = 1,
                InstanceIds = "1,2",
                IsActive = true,
                IsLocked = false,
                IsRequiredToChangePassword = false,
                LastName = "Lastname",
                MobilePhone = "111-111-1111",
                SearchSessionId = "T1",
                UserHasSystemAdminPermission = true,
                UserId = 1,
                UserIdsToExclude = "1,2",
                Username = "Username",
                WorkPhone = "111-111-1111"
            };
            return query;
        }

        /// <summary>
        /// Get SearchUserQuery with only InstanceId criteria properties set
        /// </summary>
        /// <returns>SearchUserQuery</returns>
        private SearchUserQuery GetSearchUserQueryInstanceOnlySet()
        {
            var query = new SearchUserQuery()
            {
                InstanceId = 1
            };
            return query;
        }

        /// <summary>
        /// Get SearchUserQuery with no criteria properties set
        /// </summary>
        /// <returns>SearchUserQuery</returns>
        private SearchUserQuery GetSearchUserQueryNoCriteriaSet()
        {
            return new SearchUserQuery();
        }

        #endregion
    }
}
