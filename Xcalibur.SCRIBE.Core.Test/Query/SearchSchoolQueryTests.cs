﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Core.Test.Query
{
    [TestClass]
    public class SearchSchoolQueryTests
    {
        #region Public Methods

        [TestMethod]
        public void IsCriteriaEmptyAllCriteriaSet()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptySchoolName()
        {
            var query = GetSearchSchoolQueryNoCriteriaSet();
            query.SchoolName = "SchoolName";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptySchoolNumber()
        {
            var query = GetSearchSchoolQueryNoCriteriaSet();
            query.SchoolNumber = "99999";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceAllCriteriaSet()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            Assert.AreEqual(false, query.IsCriteriaEmptyExceptInstance);
        }

        public void IsCriteriaEmptyExceptInstanceSchoolName()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            query.SchoolName = "SchoolName";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }

        [TestMethod]
        public void IsCriteriaEmptyExceptInstanceSchoolNumber()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            query.SchoolNumber = "99999";
            Assert.AreEqual(false, query.IsCriteriaEmpty);
        }



        [TestMethod]
        public void ToCriteriaString()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            const string expectedCriteriaString = "SearchSessionId - S2; " +
                                                  "DistrictValue - 1; DistrictText - DistrictName; " +
                                                  "SchoolName - SchoolName; " +
                                                  "SchoolNumber - 99999; " +
                                                  "SchooYearValue - 2019; SchooYearValue - 2019-2020; ";
            Assert.AreEqual(expectedCriteriaString, query.ToCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            const string expectedCriteriaString = "<strong>District Name:</strong> DistrictName; " +
                                                  "<strong>School Name:</strong> SchoolName; " +
                                                  "<strong>School Number:</strong> 99999; " +
                                                  "<strong>School Year:</strong> 2019; ";

            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_NoDistrict()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            query.DistrictQueryItem = null;
            const string expectedCriteriaString = "<strong>School Name:</strong> SchoolName; " +
                                                  "<strong>School Number:</strong> 99999; " +
                                                  "<strong>School Year:</strong> 2019; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }


        [TestMethod]
        public void ToFormattedCriteriaString_NoSchoolName()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            query.SchoolName = string.Empty;
            const string expectedCriteriaString = "<strong>District Name:</strong> DistrictName; " +
                                                  "<strong>School Number:</strong> 99999; " +
                                                  "<strong>School Year:</strong> 2019; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        public void ToFormattedCriteriaString_NoSchoolNumber()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            query.SchoolNumber = string.Empty;
            const string expectedCriteriaString = "<strong>District Name:</strong> DistrictName; " +
                                                  "<strong>School Name:</strong> SchoolName; " +
                                                  "<strong>School Year:</strong> 2019; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        [TestMethod]
        public void ToFormattedCriteriaString_NoSchoolYear()
        {
            var query = GetSearchSchoolQueryAllCriteriaSet();
            query.SchoolYearQueryItem = null;
            const string expectedCriteriaString = "<strong>District Name:</strong> DistrictName; " +
                                                  "<strong>School Name:</strong> SchoolName; " +
                                                  "<strong>School Number:</strong> 99999; ";
            Assert.AreEqual(expectedCriteriaString, query.ToFormattedCriteriaString());
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get SearchSchoolQuery with all criteria properties set
        /// </summary>
        /// <returns>SearchSchoolQuery</returns>
        private SearchSchoolQuery GetSearchSchoolQueryAllCriteriaSet()
        {
            var query = new SearchSchoolQuery
            {
                DistrictQueryItem = new QueryItem (1, "DistrictName"),
                InstanceId = 1,
                SchoolName = "SchoolName",
                SchoolNumber = "99999",
                SchoolYearQueryItem = new QueryItem (2019, "2019-2020"),
                SearchSessionId = "S2"

            };
            return query;
        }

        /// <summary>
        /// Get SearchSchoolQuery with only InstanceId criteria properties set
        /// </summary>
        /// <returns>SearchSchoolQuery</returns>
        private SearchSchoolQuery GetSearchSchoolQueryInstanceOnlySet()
        {
            var query = new SearchSchoolQuery()
            {
                InstanceId = 1
            };
            return query;
        }

        /// <summary>
        /// Get SearchSchoolQuery with no criteria properties set
        /// </summary>
        /// <returns>SearchSchoolQuery</returns>
        private SearchSchoolQuery GetSearchSchoolQueryNoCriteriaSet()
        {
            return new SearchSchoolQuery();
        }

        #endregion
    }
}
