﻿namespace Xcalibur.SCRIBE.Core.Settings
{
    public interface IDatabaseSettings
    {
        string CommandConnectionString { get; }
        string QueryConnectionString { get; }
    }
}
