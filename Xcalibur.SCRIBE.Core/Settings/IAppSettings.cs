﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcalibur.SCRIBE.Core.Settings
{
    public interface IAppSettings
    {
        string SCRIBEVersion { get; }
        int MaxFailedLoginAttemptsAllowed { get; }
    }
}
