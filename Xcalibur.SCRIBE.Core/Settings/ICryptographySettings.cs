﻿namespace Xcalibur.SCRIBE.Core.Settings
{
    public interface ICryptographySettings
    {
        string EncryptionKey { get; }
    }
}
