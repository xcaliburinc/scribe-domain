﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xcalibur.SCRIBE.Core.Query
{
    public class SearchCourseQuery : IQuery, ICriteria
    {
        #region Public Properties

        /// <summary>
        /// Course Level Id
        /// </summary>
        public QueryItem CourseLevelQueryItem { get; set; }

        /// <summary>
        /// Course Name
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// Course Number
        /// </summary>
        public string CourseNumber { get; set; }

        /// <summary>
        /// Course Result Group Id
        /// </summary>
        public QueryItem CourseResultGroupQueryItem { get; set; }

        /// <summary>
        /// Course Type Id
        /// </summary>
        public QueryItem CourseTypeQueryItem { get; set; }

        /// <summary>
        /// Instance Id
        /// </summary>
        public int InstanceId { get; set; }

        /// <summary>
        /// Is Not Mapped
        /// </summary>
        public bool IsNotMapped { get;  set; }

        /// <summary>
        /// School Id
        /// </summary>
        public QueryItem SchoolQueryItem { get; set; }

        /// <summary>
        /// School Year
        /// </summary>
        public QueryItem SchoolYearQueryItem { get; set; }

        /// <summary>
        /// Scoring Method Id
        /// </summary>
        public QueryItem ScoringMethodQueryItem { get; set; }


        /// <summary>
        /// Standard Course Id
        /// </summary>
        public QueryItem StandardCourseQueryItem { get; set;  }

        /// <inheritdoc />
        /// <summary>
        /// Whether Criteria is Empty (Not Including Instance Id)
        /// </summary>
        public bool IsCriteriaEmpty => IsCriteriaEmptyExceptInstance && SchoolQueryItem == null;

        /// <inheritdoc />
        /// <summary>
        /// Whether Criteria Is Empty excludes check for Instance Id and School Id
        /// </summary>
        public bool IsCriteriaEmptyExceptInstance => !(CourseNumber != null || CourseName != null || SchoolYearQueryItem.Value != CurrentSchoolYear.ToString()) && IsAdvancedCriteriaEmpty;


        public bool IsAdvancedCriteriaEmpty => !(CourseTypeQueryItem != null || CourseLevelQueryItem != null ||
                                                 CourseLevelQueryItem != null || ScoringMethodQueryItem != null ||
                                                 CourseResultGroupQueryItem != null || StandardCourseQueryItem != null);

        /// <summary>
        /// Search Session Id - used to determine whether the criteria in session is valid for given page
        /// </summary>
        public string SearchSessionId { get; set; }

        /// <summary>
        /// UserId - Used in conjuction with Permissions collection to retrieve Courses that the user has specified permissions
        /// </summary>
        public int? UserId { get;  set; }

        /// <summary>
        /// Collection of permission Ids - used in conjuction with UserId to retrieve Courses that the user has specified permissions
        /// NOTE: Pass int value not enum
        /// </summary>
        public IEnumerable<int> Permissions { get; set; }

        /// <summary>
        /// Current School Year 
        /// </summary>
        public int CurrentSchoolYear { get; set; } 

        #endregion



        #region Public Methods

        public string ToCriteriaString()
        {
            var criteria = new StringBuilder($"SearchSessionId - {SearchSessionId}; InstanceId = {InstanceId};");

            if (!string.IsNullOrEmpty(CourseName))
                criteria.Append($" Course Name - {CourseName};");
            if (!string.IsNullOrEmpty(CourseNumber))
                criteria.Append($" Course Number - {CourseNumber};");

            if (SchoolQueryItem != null)
                criteria.Append($" School Id - {SchoolQueryItem.Value};");

            if (SchoolYearQueryItem != null)
                criteria.Append($" School Year - {SchoolYearQueryItem.Value};");

            if (IsNotMapped)
                criteria.Append(" (Not Mapped to selected School/SchoolYear)");

            if (CourseTypeQueryItem != null)
                criteria.Append($" Course Type Id - {CourseTypeQueryItem.Value};");

            if (CourseLevelQueryItem != null)
                criteria.Append($" Course Level Id - {CourseLevelQueryItem.Value};");

            if (ScoringMethodQueryItem != null)
                criteria.Append($" Scoring Method Id - {ScoringMethodQueryItem.Value};");

            if (CourseResultGroupQueryItem != null)
                criteria.Append($" Course Result Group Id - {CourseResultGroupQueryItem.Value};");

            if (StandardCourseQueryItem != null)
                criteria.Append($" Standard Course Id - {StandardCourseQueryItem.Value};");

            return criteria.ToString();

        }

        public string ToFormattedCriteriaString()
        {
            var searchCriteria = new StringBuilder();

            if (!string.IsNullOrEmpty(CourseName))
                searchCriteria.Append($" <strong>Course Name:</strong> {CourseName}");
            if (!string.IsNullOrEmpty(CourseNumber))
                searchCriteria.Append($" <strong>Course Number:</strong> {CourseNumber}");

            if (SchoolQueryItem != null)
                searchCriteria.Append($" School Id - {SchoolQueryItem.Text};");

            if (SchoolYearQueryItem != null)
                searchCriteria.Append($" School Year - {SchoolYearQueryItem.Text};");

            if (IsNotMapped)
                searchCriteria.Append(" (Not Mapped to selected School/SchoolYear)");

            if (CourseTypeQueryItem != null)
                searchCriteria.Append($" Course Type - {CourseTypeQueryItem.Text};");

            if (CourseLevelQueryItem != null)
                searchCriteria.Append($" Course Level - {CourseLevelQueryItem.Text};");

            if (ScoringMethodQueryItem != null)
                searchCriteria.Append($" Scoring Method - {ScoringMethodQueryItem.Text};");

            if (CourseResultGroupQueryItem != null)
                searchCriteria.Append($" Course Result Group - {CourseResultGroupQueryItem.Text};");

            if (StandardCourseQueryItem != null)
                searchCriteria.Append($" Standard Course - {StandardCourseQueryItem.Text};");

            return searchCriteria.Length > 0 ? searchCriteria.ToString().Remove(1) : string.Empty;
        }

        #endregion



    }
}
