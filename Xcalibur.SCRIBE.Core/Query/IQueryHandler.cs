﻿using System;
using System.Collections.Generic;

namespace Xcalibur.SCRIBE.Core.Query
{
    public interface IQueryHandler<TParameter, TResult> : IDisposable
        where TResult : IQueryResult
        where TParameter : IQuery
    {
        IEnumerable<TResult> Retrieve(TParameter query);
    }
}
