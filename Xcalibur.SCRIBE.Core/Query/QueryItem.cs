﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Web.UI.WebControls;


namespace Xcalibur.SCRIBE.Core.Query
{
    public class QueryItem
    {
        #region Constructors

        /// <summary>
        /// Constructor - QueryItem
        /// </summary>
        /// <param name="value">Query Item Value</param>
        /// <param name="text">Query Item Text</param>
        public QueryItem(string value, string text)
        {
            Text = text;
            Value = value;
        }

        /// <summary>
        /// Constructor - QueryItem
        /// </summary>
        /// <param name="value">Query Item Value</param>
        /// <param name="text">Query Item Text</param>
        public QueryItem(object value, object text)
        {
            Text = text.ToString();
            Value = value.ToString();
        }

        /// <summary>
        /// Constructor - QueryItem
        /// </summary>
        /// <param name="value">Query Item Value</param>
        public QueryItem(object value)
        {
            Text = "";
            Value = value.ToString();
        }

        /// <summary>
        /// Constructor - QueryItem
        /// </summary>
        /// <param name="listItem">ListItem object</param>
        public QueryItem(ListItem listItem)
        {
            if (listItem == null)
            {
                Text = "";
                Value = "";
            }
            else
            {
                Text = listItem.Text;
                Value = listItem.Value;
            }
        }


        /// <summary>
        /// Constructor - QueryItem
        /// </summary>
        /// <param name="value">Query Item Value</param>
        public QueryItem(string value)
        {
            Value = value;
            Text = value;
        }

        /// <summary>
        /// Constructor - QueryItem
        /// </summary>
        public QueryItem()
        {
            Value = "";
            Text = "";
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Query Item Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Query Item Value
        /// </summary>
        public string Value { get; set; }

        #endregion

        #region Public Methods


        /// <summary>
        /// Convert QueryItem to ListItem object
        /// </summary>
        /// <returns>ListItem</returns>
        public ListItem ToListItem()
        {
            return new ListItem(Text, Value);
        }

        #endregion
    }

    public class QueryItemCollection : KeyedCollection<string, QueryItem>
    {
        #region Constructors

        /// <summary>
        /// Constructor  - QueryItemCollection
        /// </summary>
        /// <param name="listItems">IList of ListItem objects</param>
        public QueryItemCollection(IEnumerable<ListItem> listItems)
        {
            foreach (var li in listItems)
                Add(new QueryItem(li));
        }

        /// <summary>
        /// Constructor - CrieriaItemCollection
        /// </summary>
        public QueryItemCollection()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieve comma separated list of text elements
        /// </summary>
        /// <returns>Comma separated list of text elements</returns>
        public string ToCommaSeparatedTextList()
        {
            var sb = new StringBuilder();
            foreach (var ci in this)
            {
                if (sb.Length > 0)
                    sb.Append(",");
                sb.Append(ci.Text);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Retrieve comma separated list of values
        /// </summary>
        /// <returns>Comma seperated list of values</returns>
        public string ToCommaSeparatedValueList()
        {
            var sb = new StringBuilder();
            foreach (QueryItem ci in this)
            {
                if (sb.Length > 0)
                    sb.Append(",");
                sb.Append(ci.Value);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Convert QueryItemCollection to Collection of ListItem objects.
        /// </summary>
        /// <returns>Collection of ListItem Objects</returns>
        public System.Collections.ObjectModel.Collection<ListItem> ToListItemCollection()
        {
            var listItems = new System.Collections.ObjectModel.Collection<ListItem>();
            foreach (QueryItem ci in this)
            {
                listItems.Add(ci.ToListItem());
            }
            return listItems;
        }

        #endregion

        #region Protected Methods

        protected override string GetKeyForItem(QueryItem item)
        {
            return item.Value;
        }

        #endregion
    }
}
