﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcalibur.SCRIBE.Core.Query
{
    public interface ICriteria
    {
        #region Public Properties

        /// <summary>
        /// Whether Criteria is Empty
        /// </summary>
        bool IsCriteriaEmpty
        { get; }

        /// <summary>
        /// Whether Criteria is Empty excluding InstanceId
        /// </summary>
        bool IsCriteriaEmptyExceptInstance { get; }

        /// <summary>
        /// Search Session Id
        /// </summary>
        string SearchSessionId
        { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create String Representation of User Criteria Object
        /// </summary>
        /// <returns>String representation of User Criteria Object</returns>
        string ToCriteriaString();

        /// <summary>
        /// Get formatted Criteria String
        /// </summary>
        /// <returns>String - Formatted Criteria String</returns>
        string ToFormattedCriteriaString();

        #endregion
    }

}
