﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xcalibur.SCRIBE.Core.Domain;

namespace Xcalibur.SCRIBE.Core.Query
{
    public class SearchDistrictQuery : IQuery
    {
        #region Public Properties

        /// <summary>
        /// District Name
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// District Number
        /// </summary>
        public string DistrictNumber { get; set; }

        /// <summary>
        /// Instance Id
        /// </summary>
        public int InstanceId { get; set; }

        /// <summary>
        /// Whether Criteria is Empty
        /// </summary>
        public bool IsCriteriaEmpty => IsCriteriaEmptyExceptInstance&& InstanceId == -1;

        public bool IsCriteriaEmptyExceptInstance => !(DistrictName != null || DistrictNumber != null);


        /// <summary>
        /// Search Session Id - used to determine whether the criteria in session is valid for given page
        /// </summary>
        public string SearchSessionId { get; set; }

        /// <summary>
        /// UserId - Used in conjuction with Permissions collection to retrieve Districts that the user has specified permissions
        /// </summary>
        public int? UserId { get;  set; }

        /// <summary>
        /// Collection of permission Ids - used in conjuction with UserId to retrieve Districts that the user has specified permissions
        /// NOTE: Pass int value not enum
        /// </summary>
        public IEnumerable<int> Permissions { get; set; }

        #endregion

        #region Public Methods

        public string ToCriteriaString()
        {
            var criteria = new StringBuilder($"SearchSessionId - {SearchSessionId};");

            if (!string.IsNullOrEmpty(DistrictName))
                criteria.Append($" District Name - {DistrictName};");
            if (!string.IsNullOrEmpty(DistrictNumber))
                criteria.Append($" District Number - {DistrictNumber};");

            criteria.Append($" Instance Id - {InstanceId};");

            if (UserId != null)
                criteria.Append($" User Id - {UserId};");

            return criteria.ToString();
        }

        public string ToFormattedCriteriaString()
        {
            var searchCriteria = new StringBuilder();

            if (!string.IsNullOrEmpty(DistrictName))
                searchCriteria.Append($" <strong>District Name:</strong> {DistrictName}");
            if (!string.IsNullOrEmpty(DistrictNumber))
                searchCriteria.Append($" <strong>District Number:</strong> {DistrictNumber}");

            return searchCriteria.Length > 0 ? searchCriteria.ToString().Remove(1) : string.Empty;
        }

        #endregion

    }
}
