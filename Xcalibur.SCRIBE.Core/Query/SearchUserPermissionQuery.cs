﻿namespace Xcalibur.SCRIBE.Core.Query
{
    public class SearchUserPermissionQuery : IQuery
    {
        public int UserId { get; set; }
    }
}
