﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xcalibur.SCRIBE.Core.Domain;

namespace Xcalibur.SCRIBE.Core.Query
{
    public class SearchSchoolQuery : IQuery, ICriteria
    {
        /// <summary>
        /// District
        /// </summary>
        public QueryItem DistrictQueryItem { get; set; }

        /// <summary>
        /// Instance Id
        /// </summary>
        public int InstanceId { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Whether Criteria is Empty
        /// </summary>
        public bool IsCriteriaEmpty => IsCriteriaEmptyExceptInstance&& InstanceId == -1;

        public bool IsCriteriaEmptyExceptInstance => !(SchoolName != null || SchoolNumber != null || DistrictQueryItem != null || SchoolYearQueryItem != null);


        /// <summary>
        /// Collection of permissions - used in conjuction with UserId to retrieve Schools that the user has specified permissions
        /// NOTE: Pass int value not enum
        /// </summary>
        public IEnumerable<Permission.enmPermission> Permissions { get;  set; }

        /// <summary>
        /// School Name
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// School Number
        /// </summary>
        public string SchoolNumber { get; set; }

        /// <summary>
        /// School Year
        /// </summary>
        public QueryItem SchoolYearQueryItem { get; set; }

        /// <summary>
        /// Search Session Id - used to determine whether the Query in session is valid for given page
        /// </summary>
        public string SearchSessionId { get; set; }

        /// <summary>
        /// UserId - Used in conjuction with Permissions collection to retrieve Schools that the user has specified permissions
        /// </summary>
        public int? UserId { get; set; }


        public string ToCriteriaString()
        {
            StringBuilder criteria = new StringBuilder($" SearchSessionId - {SearchSessionId};");

            if (!string.IsNullOrEmpty(SchoolNumber))
                criteria.Append($" School Number - {SchoolNumber};");


            if (!string.IsNullOrEmpty(SchoolName))
                criteria.Append(string.Format(" School Name - {0};", SchoolName));

            if (DistrictQueryItem != null)
            {
                criteria.Append($" District Id - {DistrictQueryItem.Value};");
            }

            if (SchoolYearQueryItem != null)
                criteria.Append($" School Year - {SchoolYearQueryItem.Value};");

            criteria.Append($" Instance Id - {InstanceId};");
            criteria.Append($" User Id - {UserId};");
            criteria.Append($" Permissions - {string.Join(", ", Permissions.ToList())};");

            return criteria.ToString().TrimEnd(';');
        }


        public string ToFormattedCriteriaString()
        {
            var searchCriteria = new StringBuilder();

            if (!string.IsNullOrEmpty(SchoolNumber))
                searchCriteria.Append($" <strong>First Name:</strong> {SchoolNumber}");
            if (!string.IsNullOrEmpty(SchoolName))
                searchCriteria.Append($" <strong>Last Name:</strong> {SchoolName}");
            if (DistrictQueryItem != null)
                searchCriteria.Append($" <strong>District:</strong> {DistrictQueryItem.Text}");
            if (SchoolYearQueryItem != null)
                searchCriteria.Append($" <strong>School Year:</strong> {SchoolYearQueryItem.Text}");

            return searchCriteria.Length > 0 ? searchCriteria.ToString().Remove(1) : string.Empty;
        }


    }
}
