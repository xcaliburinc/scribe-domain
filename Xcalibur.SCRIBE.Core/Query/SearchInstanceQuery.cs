﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcalibur.SCRIBE.Core.Query
{
    public class SearchInstanceQuery : IQuery
    {
        public int? InstanceId { get; set; }
        public bool? IsActive { get; set; }
        //CDO: Add Instance Query Properties
    }
}
