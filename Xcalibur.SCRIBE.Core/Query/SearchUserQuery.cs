﻿using System;
using System.Text;

namespace Xcalibur.SCRIBE.Core.Query
{
    public class SearchUserQuery : IQuery, ICriteria
    {
        #region Public Properties

        public DateTime? DateLastLoggedInEnd { get; set; }
        public DateTime? DateLastLoggedInStart {get; set; }
        public DateTime? DateLockedEnd { get; set; }
        public DateTime? DateLockedStart { get; set; }

        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public int? InstanceId { get; set; }
        public string InstanceIds { get; set; }
        public bool? IsActive { get; set; }

        public bool IsCriteriaEmpty => !(!string.IsNullOrEmpty(FirstName) || !string.IsNullOrEmpty(LastName) || IsActive != null || IsLocked != null || DateLockedStart != null || DateLockedEnd != null || 
                                         DateLastLoggedInStart != null || DateLastLoggedInEnd != null || !string.IsNullOrEmpty(EmailAddress) || IsRequiredToChangePassword != null || InstanceId != null || !string.IsNullOrEmpty(Username)
                                         || !string.IsNullOrEmpty(WorkPhone) || !string.IsNullOrEmpty(MobilePhone) || !string.IsNullOrEmpty(InstanceIds) || UserId != null || !string.IsNullOrEmpty(UserIdsToExclude) || UserHasSystemAdminPermission != null);

        public bool IsCriteriaEmptyExceptInstance => !(!string.IsNullOrEmpty(FirstName) || !string.IsNullOrEmpty(LastName) || IsActive != null || IsLocked != null || DateLockedStart != null || DateLockedEnd != null ||
              DateLastLoggedInStart != null || DateLastLoggedInEnd != null || !string.IsNullOrEmpty(EmailAddress) || IsRequiredToChangePassword != null || !string.IsNullOrEmpty(Username)
              || !string.IsNullOrEmpty(WorkPhone) || !string.IsNullOrEmpty(MobilePhone) || !string.IsNullOrEmpty(InstanceIds) || UserId != null || !string.IsNullOrEmpty(UserIdsToExclude) || UserHasSystemAdminPermission != null);

        public bool? IsLocked { get; set; }
        public bool? IsRequiredToChangePassword { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string SearchSessionId {get; set; }
        public bool? UserHasSystemAdminPermission { get; set; }
        public int? UserId { get; set; }
        public string UserIdsToExclude { get; set; }
        public string Username { get; set; }
        public string WorkPhone { get; set; }

        #endregion

        #region Public Methods

        public string ToCriteriaString()
        {
            var criteria = new StringBuilder();
            if (!string.IsNullOrEmpty(SearchSessionId))
                criteria.Append($"SearchSessionId - {SearchSessionId}; ");
            if (!string.IsNullOrEmpty(FirstName))
                criteria.Append($"First Name - {FirstName}; ");
            if (!string.IsNullOrEmpty(LastName))
                criteria.Append($"Last Name - {LastName}; ");
            if (!string.IsNullOrEmpty(Username))
                criteria.Append($"Username - {Username}; ");
            if (!string.IsNullOrEmpty(WorkPhone))
                criteria.Append($"Work Phone - {WorkPhone}; ");
            if (!string.IsNullOrEmpty(MobilePhone))
                criteria.Append($"Mobile Phone - {MobilePhone}; ");
            if (IsActive != null)
                criteria.AppendFormat("Is Active - {0}; ", (bool) IsActive ? "Yes" : "No");
            if (IsLocked != null)
                criteria.AppendFormat("Is Locked - {0}; ", (bool) IsLocked ? "Yes" : "No");
            if (DateLockedStart != null)
                criteria.Append($"Date Locked Start - {((DateTime)DateLockedStart).ToShortDateString()}; ");
            if (DateLockedEnd != null)
                criteria.Append($"Date Locked End - {((DateTime)DateLockedEnd).ToShortDateString()}; ");
            if (DateLastLoggedInStart != null)
                criteria.Append($"Date Last Logged In Start - {((DateTime)DateLastLoggedInStart).ToShortDateString()}; ");
            if (DateLastLoggedInEnd != null)
                criteria.Append($"Date Last Logged In End - {((DateTime)DateLastLoggedInEnd).ToShortDateString()}; ");
            if (!string.IsNullOrEmpty(EmailAddress))
                criteria.Append($"Email Address - {EmailAddress}; ");
            if (IsRequiredToChangePassword != null)
                criteria.AppendFormat("Is Required To Change Password - {0}; ", (bool) IsRequiredToChangePassword ? "Yes" : "No");
            if (InstanceId != null)
                criteria.Append($"Instance Id - {InstanceId}; ");
            if (!string.IsNullOrEmpty(InstanceIds))
                criteria.Append($"Instance Ids - {InstanceIds}; ");
            if (UserId != null)
                criteria.Append($"User Id - {UserId}; ");
            if (!string.IsNullOrEmpty(UserIdsToExclude))
                criteria.Append($"User Ids to Exclude - {UserIdsToExclude}; ");
            if (UserHasSystemAdminPermission != null)
                criteria.AppendFormat("User has System Admin Permission - {0}; ", (bool) UserHasSystemAdminPermission ? "Yes" : "No");

            return criteria.ToString();
        }

        public string ToFormattedCriteriaString()
        {
            var searchCriteria = new StringBuilder();

            if (!string.IsNullOrEmpty(FirstName))
                searchCriteria.Append($" <strong>First Name:</strong> {FirstName}");
            if (!string.IsNullOrEmpty(LastName))
                searchCriteria.Append($" <strong>Last Name:</strong> {LastName}");
            if (!string.IsNullOrEmpty(Username))
                searchCriteria.Append($" <strong>Username:</strong> {Username}");
            if (!string.IsNullOrEmpty(EmailAddress))
                searchCriteria.Append($" <strong>Email:</strong> {EmailAddress}");
            if (!string.IsNullOrEmpty(WorkPhone))
                searchCriteria.Append($" <strong>Work Phone:</strong> {WorkPhone}");
            if (!string.IsNullOrEmpty(MobilePhone))
                searchCriteria.Append($" <strong>Mobile Phone:</strong> {MobilePhone}");

            if (DateLastLoggedInStart != null && DateLastLoggedInEnd != null)
                searchCriteria.Append($" <strong>Date Last Logged In:</strong> {((DateTime)DateLastLoggedInStart).ToShortDateString()} to {((DateTime)DateLastLoggedInEnd).ToShortDateString()}");
            else if (DateLastLoggedInStart != null)
                searchCriteria.Append($" <strong>Date Last Logged In:</strong> On or After {((DateTime)DateLastLoggedInStart).ToShortDateString()}");
            else if (DateLastLoggedInEnd != null)
                searchCriteria.Append($" <strong>Date Last Logged In:</strong> On or Before {((DateTime)DateLastLoggedInEnd).ToShortDateString()}");

            if (IsActive != null)
                searchCriteria.AppendFormat(" <strong>Active:</strong> {0}", (bool)IsActive ? "Yes" : "No");
            if (IsRequiredToChangePassword != null)
                searchCriteria.AppendFormat(" <strong>Required To Change Password:</strong> {0}", (bool)IsRequiredToChangePassword ? "Yes" : "No");
            if (IsLocked != null)
                searchCriteria.AppendFormat(" <strong>Locked:</strong> {0}", (bool)IsLocked ? "Yes" : "No");

            if (DateLockedStart != null && DateLockedEnd != null)
                searchCriteria.Append($" <strong>Date Locked:</strong> {((DateTime)DateLockedStart).ToShortDateString()} to {((DateTime)DateLockedEnd).ToShortDateString()}");
            else if (DateLockedStart != null)
                searchCriteria.Append($" <strong>Date Locked:</strong> On or After {((DateTime)DateLockedStart).ToShortDateString()}");
            else if (DateLockedEnd != null)
                searchCriteria.Append($" <strong>Date Locked:</strong> On or Before {((DateTime)DateLockedEnd).ToShortDateString()}");

            return searchCriteria.ToString();
        }

        #endregion
    } 
}
