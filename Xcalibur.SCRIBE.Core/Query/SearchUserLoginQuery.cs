﻿namespace Xcalibur.SCRIBE.Core.Query
{
    public class SearchUserLoginQuery : IQuery
    {
        public string Username { get; set; }
    }
}
