using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{

    [Table("Course")]
    public class CourseDto : IQueryResult
    {
        public int CourseId { get; set; }

        [Required]
        [StringLength(100)]
        public string CourseName { get; set; }

        [StringLength(100)]
        public string CourseNumber { get; set; }

        public int ScoringMethodId { get; set; }

        public int? CourseTypeId { get; set; }

        public int? CourseLevelId { get; set; }

        public int? CourseResultGroupId { get; set; }

        public int? StandardCourseId { get; set; }

        public int InstanceId { get; set; }

        public bool IsActive { get; set; }

        public DateTime DateCreated { get; set; }

        public int CreationMethodId { get; set; }

        public int CreatedByUserId { get; set; }

        public DateTime? DateLastUpdated { get; set; }

        public int? LastUpdateMethodId { get; set; }

        public int? LastUpdatedByUserId { get; set; }

        [StringLength(100)]
        public string ExternalId { get; set; }

        public decimal? Credits { get; set; }

        public decimal? FinalOverrideCredits { get; set; }
    }
}
