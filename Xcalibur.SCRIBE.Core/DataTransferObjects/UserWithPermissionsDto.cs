﻿using System.Collections.Generic;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{
    public class UserWithPermissionsDto
    {
        #region Constructors

        public UserWithPermissionsDto()
        {
            UserPermissions = new HashSet<UserPermissionDto>();
        }

        #endregion

        #region Public Properties

        public string FirstName { get; set; }

        public string FullName => string.Format("{0} {1}", FirstName, LastName);

        public int? InstanceIdDefault { get; set; }
        public string LastName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }

        public IEnumerable<UserPermissionDto> UserPermissions { get; set; }

        public bool IsSystemAdmin { get; set; }
        public bool HasExpenseReportPermissionOnly { get; set; }
        #endregion
    }
}
