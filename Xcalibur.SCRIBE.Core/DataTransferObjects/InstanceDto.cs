﻿using System;
using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{
    public class InstanceDto : IQueryResult
    {
        #region Public Properties

        public int AdditionalSpaceAllocatedKBs { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public bool AllowAddingCollegeEnrollment { get; set; }
        public int AttachmentTierId { get; set; }
        public string AttachmentTierName { get; set; }
        public string CCRECServiceAggreementNumber { get; set; }
        public int CCRECStateId { get; set; }
        public string CertifyingOfficialEmailAddress { get; set; }
        public string CertifyingOfficialName { get; set; }
        public string CertifyingOfficialPhone { get; set; }
        public string City { get; set; }
        public bool ClientPortalActive { set; get; }
        public bool ClientPortalAllowSpanish { get; set; }
        public string ClientPortalClientHomeSiteName { get; set; }
        public string ClientPortalClientHomeSiteNameSpanish { get; set; }
        public string ClientPortalClientHomeSiteURL { get; set; }
        public bool ClientPortalSurveyActive { set; get; }
        public string CreatedByUser { set; get; }
        public string CreationMethodName { set; get; }
        public string CustomCreditsLabel { set; get; }
        public string CustomDistrictIdLabel { get; set; }
        public string CustomDistrictObjectName { get; set; }
        public string CustomDistrictObjectNamePluralForm { get; set; }
        public string CustomSchoolIdLabel { get; set; }
        public string CustomSchoolObjectName { get; set; }
        public string CustomSchoolObjectNamePluralForm { get; set; }
        public string CustomStudentIdLabel { set; get; }
        public string CustomStudentIdLabelSpanish { get; set; }
        public bool DataIsImported { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateLastUpdated { set; get; }
        public int DocumentUploadSpaceUsedKBs { get; set; }
        public decimal ExceedExpectationsPercentage { set; get; }
        public string ExpenseReportApprovalStatusCustomName { get; set; }
        public string ExpenseReportDraftStatusCustomName { get; set; }
        public string ExpenseReportRejectionStatusCustomName { get; set; }
        public decimal IndirectCostPercentage { get; set; }
        public int InstanceId { get; set; }
        public string InstanceName { get; set; }
        public bool IsActEngageTracked { get; set; }
        public bool IsActive { set; get; }
        public bool IsDocumentUploadActive { get; set; }
        public bool IsElementaryImplementation { get; set; } //CDO: Update database IsElementaryImplementation should not allow null
        public bool IsGeocodingActive { get; set; }
        public bool IsHighSchoolImplementation { get; set; } //CDO: Update database IsHighSchoolImplementation should not allow null
        public bool IsMastersImplementation { get; set; } //CDO: Update database IsMastersImplementation should not allow null
        public bool IsPostDoctorateImplementation { get; set; } //CDO: Update database IsPostDoctorateImplementation should not allow null
        public bool IsRequiredToCertifyService { set; get; }
        public bool IsSchoolStudentIdRequired { get; set; }
        public bool IsSurveyBirthdateRequired { get; set; }
        public bool IsSurveyStudentIdRequired { get; set; }
        public bool IsUnderGraduateImplementation { get; set; } //CDO: Update database IsUnderGraduateImplementation should not allow null
        public string LastUpdatedByUser { set; get; }
        public string LastUpdateMethodName { set; get; }
        public decimal MileageReimbursement { get; set; }
        public string NSCAccountNumber { get; set; }
        public int NSCImportFormatId { get; set; }
        public string NSCOrganizationName { get; set; }
        public int NumberSCRIBEUserLicenses { get; set; }
        public decimal PassingPercentage { get; set; }
        public string PrAwardNumber { get; set; }
        public string ProjectDirectorEmailAddress { get; set; }
        public string ProjectDirectorFax { get; set; }
        public string ProjectDirectorName { get; set; }
        public string ProjectDirectorPhone { get; set; }
        public string ServiceCertificationText { set; get; }
        public bool ShowCollegeEnrollmentModule { get; set; }
        public bool ShowExpenseReportModule { get; set; }
        public bool ShowMatchTrackingModule { get; set; }
        public bool ShowServiceCategories { set; get; }
        public bool ShowServiceFundingModule { set; get; }
        public bool ShowSSNField { get; set; }
        public bool ShowSystemServiceSummaries { get; set; } //CDO: Update database ShowSystemServicesSummaries should not allow null
        public string StateAbbreviation { get; set; }
        public string StateName { get; set; }
        public int StateId { get; set; }
        public string StudentReleaseFormLabel { set; get; }
        public string SurveyIdVerificationCustomParentInstructions { get; set; }
        public string SurveyIdVerificationCustomParentInstructionsSpanish { get; set; }
        public string SurveyIdVerificationCustomStudentInstructions { get; set; }
        public string SurveyIdVerificationCustomStudentInstructionsSpanish { get; set; }
        public string SurveyIdVerificationCustomTeacherInstructions { get; set; }
        public string SurveyIdVerificationCustomTeacherInstructionsSpanish { get; set; }
        public int? SurveyStudentIdMatchMinimalCharacters { get; set; }
        public int? SurveyStudentIdMatchOptionsId { set; get; }
        public string SurveyStudentIdMatchOptionsName { set; get; }
        public string ZipCode { get; set; }

        #endregion
    }
}
