﻿using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{
    public class UserPermissionDto : IQueryResult
    {
        public int EntityTypeId { get; set; }
        public int EntityId { get; set; }
        public int PermissionId { get; set; }
        public int InstanceId { get; set; }
        public int DistrictId { get; set; }
    }
}
