﻿using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{
    public class UserLoginDto : IQueryResult
    {
        public int UserId { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public bool IsLocked { get; set; }

        public bool IsActive { get; set; }

        public string EmailAddress { get; set; }

        public bool IsRequiredToChangePassword { get; set; }

        public bool IsPasswordValid { get; set; }
    }
}
