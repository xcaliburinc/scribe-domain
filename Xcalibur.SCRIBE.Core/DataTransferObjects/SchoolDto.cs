using System;
using Xcalibur.SCRIBE.Core.Query;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{
    [Table("School")]
    public class SchoolDto : IQueryResult
    {
        public int SchoolId { get; set; }

        [Required]
        [StringLength(255)]
        public string SchoolName { get; set; }

        [StringLength(100)]
        public string SchoolNumber { get; set; }

        [StringLength(150)]
        public string Address1 { get; set; }

        [StringLength(150)]
        public string Address2 { get; set; }

        [Required]
        [StringLength(100)]
        public string City { get; set; }

        public int StateId { get; set; }

        [Required]
        [StringLength(20)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        public int DistrictId { get; set; }

        public int? ProgramMinGradeLevelId { get; set; }

        public int? ProgramMaxGradeLevelId { get; set; }

        public int ActualMinGradeLevelId { get; set; }

        public int ActualMaxGradeLevelId { get; set; }

        public int InstanceId { get; set; }

        public bool IsActive { get; set; }

        public System.DateTime DateCreated { get; set; }

        public int CreationMethodId { get; set; }

        public int CreatedByUserId { get; set; }

        public DateTime? DateLastUpdated { get; set; }

        public int? LastUpdateMethodId { get; set; }

        public int? LastUpdatedByUserId { get; set; }

        [StringLength(100)]
        public string ExternalId { get; set; }

        [StringLength(50)]
        public string NCESId { get; set; }

        [StringLength(500)]
        public string SchoolNotes { get; set; }

        [StringLength(50)]
        public string ACTCode { get; set; }

        public int? SchoolTypeId { get; set; }

        public int? SchoolSectorId { get; set; }
    }
}
