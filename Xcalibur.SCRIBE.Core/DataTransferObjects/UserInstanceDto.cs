﻿using System;
using Xcalibur.SCRIBE.Core.Query;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{
    public class UserInstanceDto : IQueryResult
    {
        #region Public Properties

        public bool? CanRequestPassword { get; set; }
        public int? CreatedByUserId { get; set; }
        public string CreatedByUserName { get; set; }
        public int? CreationMethodId { get; set; }
        public string CreationMethodName { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateLastLogin { get; set; }
        public DateTime? DateLastUpdated { get; set; }
        public DateTime? DateLocked { get; set; }
        public string EmailAddress { get; set; }
        public int? FailedLoginAttempts { get; set; }
        public string FirstName { get; set; }
        public int? InstanceId { get; set; }
        public string InstanceName { get; set; }
        public string IPAddressLastLogin { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDefaultInstance { get; set; }
        public bool? IsLocked { get; set; }
        public bool? IsRequiredToChangePassword { get; set; }
        public string LastName { get; set; }
        public int? LastUpdatedByUserId { get; set; }
        public int? LastUpdateMethodId { get; set; }
        public string LastUpdateMethodName { get; set; }
        public string MiddleName { get; set; }
        public string MobilePhone { get; set; }
        public string ReasonLocked { get; set; }
        public string UpdatedByUserName { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string WorkPhone { get; set; }

        #endregion
    }
}
