using System;
using Xcalibur.SCRIBE.Core.Query;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xcalibur.SCRIBE.Core.DataTransferObjects
{


    [Table("District")]
    public class DistrictDto : IQueryResult
    {
        public int DistrictId { get; set; }

        [Required]
        [StringLength(100)]
        public string DistrictName { get; set; }

        [StringLength(100)]
        public string DistrictNumber { get; set; }

        public int InstanceId { get; set; }

        public bool IsActive { get; set; }

        public DateTime DateCreated { get; set; }

        public int CreationMethodId { get; set; }

        public int CreatedByUserId { get; set; }

        public DateTime? DateLastUpdated { get; set; }

        public int? LastUpdateMethodId { get; set; }

        public int? LastUpdatedByUserId { get; set; }

        [StringLength(50)]
        public string NCESId { get; set; }

    }
}
