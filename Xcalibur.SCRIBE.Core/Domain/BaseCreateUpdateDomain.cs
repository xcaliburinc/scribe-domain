﻿using System;

namespace Xcalibur.SCRIBE.Core.Domain
{
    public abstract class BaseCreateUpdateDomain
    {
        #region Public Properties

        public int CreatedByUserId { get; set; }
        public int CreationMethodId { get; set; }
        public DateTime DateCreated { get; set; }

        public DateTime? DateLastUpdated { get; set; }
        public int? LastUpdatedByUserId { get; set; }
        public int? LastUpdateMethodId { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update modification tracking information
        /// </summary>
        /// <param name="isCreated">Whether being created</param>
        /// <param name="userId">Id of User making modification</param>
        public void UpdateModificationInfo(bool isCreated, int userId)
        {
            int modificationMethodId = ModificationMethod.GetIdFromEnum(ModificationMethod.enmModificationMethod.Import);

            if (isCreated)
            {

                CreatedByUserId = userId;
                CreationMethodId = modificationMethodId;
                DateCreated = DateTime.Now;
            }

            LastUpdatedByUserId = userId;
            LastUpdateMethodId = modificationMethodId;
            DateLastUpdated = DateTime.Now;
        }

        #endregion
    }
}

