﻿namespace Xcalibur.SCRIBE.Core.Domain
{
    public class EntityType
    {
        #region Public Methods

        /// <summary>
        /// Get Id for EntityType enum
        /// </summary>
        /// <param name="entityType">Entity Type enum</param>
        /// <returns></returns>
        public static int GetIdFromEnum(enmEntityTypeDB entityType)
        {
            return (int)entityType;
        }

        #endregion


        public enum enmEntityTypeDB
        {
            System = 1,
            Instance = 2,
            District = 3,
            School = 4,
            Service = 5,
            Student = 6,
            Teacher = 7
        }
    }
}
