﻿namespace Xcalibur.SCRIBE.Core.Domain
{
    public class Enums
    {
        #region Public Methods

        ///// <summary>
        ///// Get Id for EntityType enum
        ///// </summary>
        ///// <param name="entityType">Entity Type enum</param>
        ///// <returns></returns>
        //public static int GetIdFromEnum(enmEntityTypeDB entityType)
        //{
        //    return (int)entityType;
        //}

        #endregion

        public enum enmRecordStatus
        {
            Active = 1,
            InActive = 0,
            AllRecords = -1
        }

        public enum enmActiveStatus
        {
            Active = 1,
            InActive = 0,
            AllRecords = -1
        }

        /// <summary>
        /// Implementation Types
        /// </summary>
        public enum enmImplementationType
        {
            Elementary,
            HighSchool,
            Undergraduate,
            Masters,
            PostDoctorate
        }

        /// <summary>
        /// Attachment Entity Types
        /// </summary>
        public enum enmAttachmentEntityType
        {
            Instance = 1,
            District = 2,
            School = 3,
            Person = 4,
            User = 5,
            Service = 6,
            Service_Template = 7,
            Import = 8,
            Partner = 9,
            Staff = 10,
            Expense_Report = 11
        }
    }
}
