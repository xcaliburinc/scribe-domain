﻿namespace Xcalibur.SCRIBE.Core.Domain
{
    public class ModificationMethod
    {
        #region Public Methods

        /// <summary>
        /// Get Id for Modification Method enum
        /// </summary>
        /// <param name="method">Modification Method enum</param>
        /// <returns></returns>
        public static int GetIdFromEnum(enmModificationMethod method)
        {
            return (int)method;
        }

        #endregion

        public enum enmModificationMethod
        {
            Import = 1,
            System = 2,
            AdminQuery = 3
        }
    }
}
