﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Xcalibur.SCRIBE.Core.Domain
{
    public class User : BaseCreateUpdateDomain
    {
        #region Constructors

        public User()
        {
            IsActive = true;
            IsRequiredToChangePassword = false;
            IsLocked = false;
            CanRequestPassword = true;
        }

        #endregion

        #region Public Properties

        [Required]
        public bool CanRequestPassword { get; set; }

        public DateTime? DateLastLogin { get; set; }

        public DateTime? DateLocked { get; set; }

        [Required]
        [MaxLength(100)]
        public string EmailAddress { get; set; }

        public int FailedLoginAttempts { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string IPAddressLastLogin { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public bool IsLocked { get; set; }

        [Required]        
        public bool IsRequiredToChangePassword { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [MaxLength(20)]
        public string MobilePhone { get; set; }

        [Required]
        [MaxLength(100)]
        public string Password { get; set; }

        [MaxLength(200)]
        public string ReasonLocked { get; set; }

        [Required]
        [MaxLength(100)]
        public string Salt { get; set; }

        [Key]
        public int UserId { get; set; }

        [Required]
        [MaxLength(100)]
        public string UserName { get; set; }

        [MaxLength(20)]
        public string WorkPhone { get; set; }

        #endregion
    }
}
