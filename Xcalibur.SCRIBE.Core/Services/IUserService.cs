﻿using Xcalibur.SCRIBE.Core.DataTransferObjects;

namespace Xcalibur.SCRIBE.Core.Services
{
    public interface IUserService
    {
        UserDto GetUser(string username);

        UserDto GetUser(int userId);

        UserWithPermissionsDto GetUserWithPermissions(int userId);

        UserWithPermissionsDto GetUserWithPermissions(string username);

        UserWithPermissionsDto UpdateDefaultInstanceAndGetUser(int userId, int instanceId);

        UserLoginDto ValidateUserLogin(string username, string password);

        /// <summary>
        /// Record a successful login
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="ipAddessLastLogin">IP address of user</param>
        void RecordSuccessfulLogin(int userId, string ipAddessLastLogin);

        /// <summary>
        /// Record a failed login attempt.
        /// </summary>
        /// <param name="userId">Id of User</param>
        /// <param name="maxFailedAttemptsAllowed">Max number of failed login attempts until user account is locked</param>
        /// <returns>Whether failed login attempt resulting in the user account being locked</returns>
        bool RecordFailedLogin(int userId, int maxFailedAttemptsAllowed);
    }
}
