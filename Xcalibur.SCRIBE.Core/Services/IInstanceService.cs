﻿using Xcalibur.SCRIBE.Core.DataTransferObjects;

namespace Xcalibur.SCRIBE.Core.Services
{
    public interface IInstanceService
    {
        InstanceDto GetInstance(int instanceId);
    }
}
